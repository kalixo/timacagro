<?php get_header() ?>

	<div class="page">

		<?php get_template_part( 'template-parts/header', '404' ) ?>

		<?php get_template_part( 'template-parts/content', '404' ) ?>

	</div>

<?php get_footer() ?>