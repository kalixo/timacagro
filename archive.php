<?php get_header() ?>

    <!-- Archive -->
    <?php if ( have_posts() ) : ?>

        <div class="page">

            <?php get_template_part( 'template-parts/header', 'archive' ) ?>

            <div class="l-section l-section--stack">
                <div class="l-container">

                    <?php if ( have_posts() ) : ?>

                        <div class="cards cards-center">

                            <?php while ( have_posts() ) : the_post() ?>

                                <?php get_template_part( 'template-parts/loop' ) ?>

                            <?php endwhile ?>

                        </div>

                        <?php the_posts_pagination() ?>

                    <?php endif ?>

                </div>
            </div>
        </div>

    <?php endif  ?>

<?php get_footer() ?>
