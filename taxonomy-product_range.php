<?php get_header() ?>

	<!-- Taxonomie product range -->

	<div class="page taxonomy">

		<?php get_template_part( 'template-parts/header', 'taxonomy' ) ?>

		<?php get_template_part( 'template-parts/content-taxonomy', 'product_range' ) ?>

	</div>

<?php get_footer() ?>