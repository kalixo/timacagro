<?php get_header() ?>

    <div class="page">

        <?php get_template_part( 'template-parts/header-archive', 'product' ) ?>

        <div id="product-search-result">

            <?php get_template_part( 'template-parts/content-archive', 'product' ) ?>

            <!-- Archive product -->
            <?php if ( have_posts() ) : ?>

                <div class="l-section l-section--stack">

                    <div class="l-container">

                        <div class="cards cards-center">

                            <?php while ( have_posts() ) : the_post() ?>

                                <?php get_template_part( 'template-parts/loop', 'product' ) ?>

                            <?php endwhile ?>

                        </div>

                    </div>

                </div>

                <?php the_posts_pagination() ?>
            <?php else : ?>

                <div class="l-section l-section--stack">
                    <div class="l-container">
                        <div class="text text--center h-color--gray">
                            <?php _e( 'Aucun produit ne correspond à votre filtre', 'timacagro' ) ?>
                        </div>
                    </div>
                </div>

            <?php endif  ?>

        </div>

    </div>

<?php get_footer() ?>
