<?php
/**
 * The template for displaying the header
 *
 * @package WordPress
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-33854273-1', 'auto');
        ga('send', 'pageview');
    </script>
</head>

<body <?php body_class(); ?>>
<div id="cookies-toolbar">
    <span>Le site timacagro.fr utilise des cookies afin d'améliorer votre expérience de navigation. En poursuivant votre navigation sur ce site, vous en acceptez l'utilisation.</span>
    <a href="#" class="close-ctoolbar ctoolbar-link" title="Fermer ce bandeau">
        <span>Fermer</span>
    </a>
</div>
<div id="page" class="site">

    <?php if ( ! get_field( 'lp-hide-menu' ) ) : ?>

        <header class="site__header">
            <div class="l-container">
                <?php if ( $image = get_field( 'landing-page-logo', 'options' ) ) : ?>
                    <a href="<?php bloginfo('url') ?>" class="site__home site__home--visible" title="<?php bloginfo('name') ?>">
                        <?php echo wp_get_attachment_image( $image, 'full' ) ?>
                    </a>
                <?php endif ?>

                <?php if ( have_rows( 'lp-menu' ) ) : ?>

                    <a class="menu__toggle js-toggle" href="#menu-landing-page"><i class="fa fa-bars"></i><?php _e( 'Menu', 'timacagro' ) ?></a>

                    <ul class="menu menu--header site__navigation landing" id="menu-landing-page">

                        <?php while ( have_rows( 'lp-menu' ) ) : the_row() ?>
                            <li class="menu-item">
                                <a class="js-intern-link" href="<?php the_sub_field('link') ?>" <?php if ( get_sub_field( 'target' ) ) echo 'target="_blank"' ?>><?php the_sub_field( 'label' ) ?></a>
                            </li>
                        <?php endwhile ?>

                    </ul>

                <?php endif ?>

            </div>

        </header>
    <?php endif ?>

    <div class="site__content">
