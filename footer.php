<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 */
?>

	</div><!-- .site__content -->

	<footer class="site__footer footer" role="contentinfo">
		<div class="l-container">
			<div class="footer__prefooter">
				<div class="footer__baseline">
					<a href="<?php the_field( 'footer-link', 'options' ) ?>" target="_blank"
						onclick="ga('send','event','lien-footer-groupe-roullier','clic','clic-sortant',1);">
						<?php if ( get_field( 'footer-logo', 'options' ) ) : ?>
							<?php echo wp_get_attachment_image( get_field( 'footer-logo', 'options' ), 'full' ) ?>
						<?php endif ?>
						<span><?php the_field( 'footer-baseline', 'options' ) ?></span>
					</a>
				</div>
				<?php if ( have_rows( 'footer-socials', 'options' ) ) : ?>
					<ul class="footer__social social">
						<?php while ( have_rows( 'footer-socials', 'options' ) ) : the_row() ?>
							<li class="social__item">
								<a href="<?php the_sub_field( 'link' ) ?>" title="<?php the_sub_field( 'name' ) ?>" target="_blank"
									onclick="ga('send','event','lien-footer-<?php echo strtolower( get_sub_field( 'name' ) ) ?>','clic','clic-sortant',1);">
									<span class="icon icon--<?php the_sub_field( 'icon' ) ?>"></span>
								</a>
							</li>
						<?php endwhile ?>
					</ul>
				<?php endif ?>
				<nav class="footer__nav">
					<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'menu menu--inline' ) ); ?>
				</nav>
			</div>
		</div>
		<div class="footer__copyright">
			<?php the_field( 'footer-copyright', 'options' ) ?>
		</div>
	</footer><!-- .site__footer -->
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
