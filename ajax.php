<?php
/**
 * Template Name: Page AJAX
 */

$ajax = false;
if( ! empty( $_SERVER[ 'HTTP_X_REQUESTED_WITH' ] ) &&
      strtolower( $_SERVER[ 'HTTP_X_REQUESTED_WITH' ]) == 'xmlhttprequest' ) {
    $ajax = true;
}

if ( ! $ajax) { get_header(); }

if ( have_posts() ) : ?>

	<div class="<?php if ( $ajax ) echo 'popup' ?> page">

		<?php while ( have_posts() ) : the_post() ?>

			<?php get_template_part( 'template-parts/content' ) ?>

		<?php endwhile ?>

	</div>

<?php endif ;

if ( ! $ajax ) { get_footer(); }
