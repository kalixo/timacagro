(function($){
	$(function(){
		/**
		 * Sliders
		 */
        $('.js-slider').each(function(){
            var $this = $(this);

            $this.slick({
                autoplay: true,
                autoplaySpeed: 4000,
                asNavFor: $this.data('target')
            });
        });

        $('.js-slider-tab').each(function(){
            var $this = $(this);

            $this.slick({
                asNavFor: $this.data('target'),
                slidesToShow: $this.data('number'),
                focusOnSelect: true,
                arrow: false,
                responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 1,
                        dots: true,
                        arrow: true
                    }
                }]
            });
        });


		/**
		 * Popup
		 */
		$('.js-popin').magnificPopup({
            callbacks: {
                ajaxContentAdded: function(mfpResponse) {
                    _wpcf7.supportHtml5 = $.wpcf7SupportHtml5();
                    $('div.wpcf7 > form').wpcf7InitForm();
                }
            }
        });

        /**
         * Ouverture automatique de la popup de démo si le hash "#open-demo" est présent dans l'URL.
         */
        if ( 'open-demo' == window.location.hash.substring(1) ) {
            $('.product__demo').click();
        }

        /**
         * Toggle
         */
        $('body').on('click', '.js-toggle', function(e) {
            e.preventDefault();

            var $this = $(this);
                $target = $($this.attr('href'));

            $target.toggleClass('toggle');
        });

        /**
         * Grille masonry
         */
        $(document).on('product-filter-result', function(){
            var $grid = $('.l-grid--masonry').imagesLoaded( function() {
                $grid.masonry({
                    // set itemSelector so .grid-sizer is not used in layout
                    itemSelector: '.l-column',
                    percentPosition: true
                });
            });
        }).trigger('product-filter-result');

        /**
         * Liens interne
         */
        $('.js-intern-link').click(function(e){
            e.preventDefault();

            var the_id = $(this).attr("href");

            $('html, body').animate({
                scrollTop: $(the_id).offset().top - 90
            }, 300);
        })

        $('a').on('click',function (e) {

            var current = window.location.href.split('#')[0];
            var goto = $(this).attr('href').split('#')[0];

            if ( ( current == goto || '' == goto ) && this.hash ) {
                e.preventDefault();

                var target = this.hash;
                var $target = $(target);

                if($target.length){
                    $('html,body').stop().animate({
                        'scrollTop': $target.offset().top - 90
                    }, 300);
                }
            }
        });

		/**
         * Tabulation
         */
        $('.tabs').on('click', '.tabs__anchor', function(e){
            var tab = $(e.delegateTarget),
                link = $(this),
                target = $(link.attr('href'));

            tab.find('.tabs__anchor').not(link).removeClass('tabs__anchor--active');
            tab.find('.tabs__content').not(target).removeClass('tabs__content--active');

            link.addClass('tabs__anchor--active');
            target.addClass('tabs__content--active');

            e.preventDefault();
        });

        /**
         * Filtre de produits
         */
        $('body').on('change', '.js-ajax-form', function(e) {
            $(this).trigger('submit');
        });

        $('body').on('submit', '.js-ajax-form', function(e) {
            e.preventDefault();

            var $this = $(this);
            $('#product-filter-result').fadeTo(200, 0);

            $.ajax({
                url: $this.attr('action'),
                type: $this.attr('method'),
                data: $this.serialize(),
                success: function(result) {
                    var destination = $('#product-filter-result');
                    destination.html(
                        $(result).find('#product-filter-result').html()
                    ).trigger('product-filter-result');

                    destination.fadeTo(200, 1);

                }
            })
        });

        /**
         * Recherche de produits
         */
        $('body').on('change', '.js-ajax-search', function(e) {
            var $this = $(this);

            $this.addClass('search--loading');

            $.ajax({
                url: $this.attr('action'),
                type: $this.attr('method'),
                data: $this.serialize(),
                success: function(result) {
                    $('.js-ajax-search').html(
                        $(result).find('.js-ajax-search').html()
                    ).removeClass('search--loading');
                }
            })
        });

        /**
         * Gestion de la cookie Toolbar
         */
        function getCookie(name) {
            var re = new RegExp(name + "=([^;]+)");
            var value = re.exec(document.cookie);
            return (value != null) ? unescape(value[1]) : null;
        }
        function setCookie(name, value) {
            var expires = new Date();
            expires.setTime(expires.getTime() + (365 * 24 * 60 * 60 * 1000));
            document.cookie = name + '=' + value + ';expires=' + expires.toUTCString() + '; path=/';
        }

        var ctoolbarIsHidden = getCookie('hide-ctoolbar');
        if( ctoolbarIsHidden === null ) {
            $("#cookies-toolbar").fadeIn();
        }
        $("#cookies-toolbar a.close-ctoolbar").on('click', function(evt) {
            evt.preventDefault();
            setCookie('hide-ctoolbar', 'yes');
            $("#cookies-toolbar").fadeOut();
	});
        $("a").not('.ctoolbar-link').on('click', function(evt) {
            setCookie('hide-ctoolbar', 'yes');
        });

    });
})(jQuery);