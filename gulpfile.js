var gulp         = require('gulp');
var sass         = require('gulp-sass');
var plumber      = require('gulp-plumber');
var livereload   = require('gulp-livereload');
var autoprefixer = require('gulp-autoprefixer');


gulp.task('sass', function() {
    gulp.src('./sass/*.scss')
        .pipe(plumber())
        .pipe(sass())
        .pipe(gulp.dest('.'))
        .pipe(livereload())
        ;
});

gulp.task('watch', function() {
    livereload.listen({port: 35729});
    gulp.watch('./sass/**/*.scss', ['sass']);
});

gulp.task('default', ['sass', 'watch']);