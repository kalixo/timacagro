<?php get_header() ?>

	<!-- Index -->
	<?php if ( have_posts() ) : ?>

		<div class="page">

			<?php while ( have_posts() ) : the_post() ?>

				<?php get_template_part( 'template-parts/loop' ) ?>

			<?php endwhile ?>

		</div>

		<?php the_posts_pagination() ?>

	<?php endif  ?>

<?php get_footer() ?>