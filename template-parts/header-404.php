<div class="page__header header h-background h-background--gray">
	<div class="l-container">
		<div class="header__top">

			<?php if ( function_exists( 'bcn_display' ) ) : ?>
				<!-- Fil d'ariane -->
				<div class="header__breadcrumb breadcrumb" typeof="BreadcrumbList" vocab="http://schema.org/">
					<?php bcn_display() ?>
				</div>
				<!-- / Fil d'ariane -->
			<?php endif ?>

		</div>

		<div class="header__content">
			<h1 class="header__title page__title"><?php the_field( '404-title', 'option' ) ?></h1>
		</div>
	</div>
</div>
