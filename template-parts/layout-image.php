<figure class="image">

	<?php if ( get_sub_field( 'image' ) ) : ?>
		<?php echo wp_get_attachment_image( get_sub_field( 'image' ), 'full', false, array( 'class' => 'image__image' ) ) ?>
	<?php endif ?>

	<?php if ( get_sub_field( 'title' ) || get_sub_field( 'legend' ) ) : ?>

		<figcaption class="image__caption">

			<?php if ( get_sub_field( 'title' ) ) : ?>
				<strong class="image__title"><?php the_sub_field( 'title' ) ?></strong>
			<?php endif ?>

			<?php if ( get_sub_field( 'legend' ) ) : ?>
				<div class="image__legend"> <?php the_sub_field( 'legend' ) ?> </div>
			<?php endif ?>

		</figcaption>

	<?php endif ?>

</figure>