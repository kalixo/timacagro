<?php
$classes_section   = array( 'l-section' );
$classes_container = array( 'l-container' );
$styles = array();

// Affichage en pleine largeur
if (get_sub_field('width') && get_field('type_de_layout') && get_field('type_de_layout') != 'mosaic' ) {
	$classes_container[] = 'l-container--fullwidth';
}

// Espace intérieur
if ( get_sub_field( 'padding' ) ) {
	$classes_section[] = 'l-section--space';
}

// Espace extérieur
if ( get_sub_field( 'margin' ) ) {
	$classes_section[] = 'l-section--stack';
}

// Identifiant
$identifiant = uniqid();
if ( get_sub_field( 'identifiant' ) ) {
	$identifiant = sanitize_title( get_sub_field( 'identifiant' ) );
}

// Arrière plan
$background = get_sub_field( 'background' );
if ( 'color' === $background ) {
	// En couleur
	$classes_section[] = 'h-background';
	$classes_section[] = 'h-background--effect';
	if ( get_sub_field( 'background-color' ) ) {
		$classes_section[] = 'h-background--' . get_sub_field( 'background-color' );
	}
} elseif ( 'image' === $background ) {
	// En image
	$classes_section[] = 'h-background';
	if ( get_sub_field( 'background-image' ) ) {
		$classes_section[] = 'h-background--gray';
		$classes_section[] = 'h-background--effect';
		$classes_section[] = 'h-background--image';
		$styles[] = 'background-image: url(' . wp_get_attachment_image_url( get_sub_field( 'background-image' ), 'full' ) . ')';
	}
}

?>
<div class="<?php echo implode( ' ', $classes_section ) ?>" id="<?php echo $identifiant ?>" style="<?php echo implode( ';', $styles ) ?>">

	<div class="<?php echo implode( ' ', $classes_container ) ?>">

		<?php if ( have_rows( 'columns' ) ) : ?>

			<div class="l-grid l-grid--space">

				<?php while ( have_rows( 'columns' ) ) : the_row() ?>

					<?php get_template_part( 'template-parts/layout', 'column' ) ?>

				<?php endwhile ?>

			</div>

		<?php endif ?>

	</div>

</div>
