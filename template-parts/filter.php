<?php
$product_need = timacagro_get_product_need_filter();
$product_profile = timacagro_get_product_profile_filter();

// Liste des produits concernés par la vue en cours
$post_ids = get_posts( array(
	'post_type'       => 'product',
	'posts_per_page'  => -1,
	'fields'          => 'ids',
	'tax_query'       => timacagro_product_tax_query(),
) );

// Liste des profiles associés
$profiles = wp_get_object_terms( $post_ids, 'product_profile', array( 'fields' => 'ids' ) );
$profiles = get_terms( array( 'taxonomy' => 'product_profile', 'include' => $profiles, 'fields' => 'ids' ) );

// Si un besoin est sélectionné, on ne récupère que les profiles associés à ce besoin
if ( $product_need ) {
	$selected_need = get_term_by( 'slug', $product_need, 'product_need' );
	if ( $related_profiles = get_field( 'related-taxonomies', $selected_need ) ) {
		$profiles = array_intersect( $profiles, $related_profiles );
	}
}

// Liste des besoins
$direct_needs = wp_get_object_terms( $post_ids, 'product_need', array( 'fields' => 'ids' ) );

// Si un profile est sélectionné, on ne récupère que les besoins associés à ce profile
if ( $product_profile ) {
	$selected_profile = get_term_by( 'slug', $product_profile, 'product_profile' );
	if ( $related_needs = get_field( 'related-taxonomies', $selected_profile ) ) {
		$direct_needs = array_intersect( $direct_needs, $related_needs );
	}
}

$needs = $direct_needs;
foreach ( $direct_needs as $need ) {
	$needs = array_merge(
		$needs,
		get_ancestors( $need, 'product_need', 'taxonomy' )
	);
}
$needs = array_unique( $needs );

$parent_needs = get_terms( array( 'taxonomy' => 'product_need', 'parent' => 0, 'include' => $needs ) );


if ( ( ! is_wp_error( $profiles ) && $profiles ) || ( ! is_wp_error( $needs ) && $needs ) ) : ?>

	<div class="product-filter">
		<form class="product-filter__form js-ajax-form" action="" method="get">
			<label class="product-filter__label">
				<?php _e( 'Filtrer par :', 'timacagro' ) ?>
			</label>

			<?php if ( ! is_wp_error( $profiles ) && $profiles ) : ?>
				<select name="product_profile" id="" class="product-filter__select">
					<option value="" class="option option--placeholder"><?php _e( 'Tous les profils', 'timacagro' ) ?></option>

					<?php foreach ( $profiles as $term_id ) : $term = get_term( $term_id, 'product_profile' ) ?>

						<option value="<?php echo $term->slug ?>" <?php if ( $term->slug == $product_profile ) echo 'selected="selected"' ?>><?php echo $term->name ?></option>

					<?php endforeach ?>

				</select>
			<?php endif ?>

			<?php if ( ! is_wp_error( $needs ) && $needs ) : ?>
				<select name="product_need" id="" class="product-filter__select">
					<option value="" class="option option--placeholder"><?php _e( 'Tous les besoins', 'timacagro' ) ?></option>

					<?php foreach ( $parent_needs as $parent ) {

						// Les enfants sélectionnables du besoin parcouru
						$children = get_terms( array( 'taxonomy' => 'product_need', 'parent' => $parent->term_id, 'include' => $needs ) );

						// Il a des enfants
						if ( ! is_wp_error( $children ) && ( $children || in_array( $parent->term_id, $needs ) ) ) {
							// On affiche le parent
							$selected = ( $parent->slug == $product_need ) ? 'selected="selected"' : '';
							$disabled = ( ! in_array( $parent->term_id, $direct_needs ) ) ? 'disabled' : '';

							echo sprintf(
								'<option value="%s" class="option option--parent" %s %s>%s</option>',
								$parent->slug,
								$disabled,
								$selected,
								$parent->name
							) ;

							foreach ( $children as $child ) {
								// Les enfants sélectionnables du besoin parcouru
								$grand_children = get_terms( array( 'taxonomy' => 'product_need', 'parent' => $child->term_id, 'include' => $needs ) );

								// Il a des enfants
								if ( ! is_wp_error( $grand_children ) && ( $grand_children || in_array( $child->term_id, $needs ) ) ) {
									// On affiche le parent
									$selected = ( $child->slug == $product_need ) ? 'selected="selected"' : '';
									$disabled = ( ! in_array( $child->term_id, $direct_needs ) ) ? 'disabled' : '';

									echo sprintf(
										'<option value="%s" class="option option--parent" %s %s>&ensp;%s</option>',
										$child->slug,
										$disabled,
										$selected,
										$child->name
									) ;
									foreach ( $grand_children as $grand_child ) {
										$selected = ( $grand_child->slug == $product_need ) ? 'selected="selected"' : '';

										echo sprintf(
											'<option value="%s" class="option option--child" %s>&ensp;&ensp;%s</option>',
											$grand_child->slug,
											$selected,
											$grand_child->name
										) ;
									}
								}
							}
						}
					} ?>

				</select>
			<?php endif ?>

			<input type="submit" value="Filtrer" class="product-filter__submit">
		</form>
	</div>

<?php endif ?>
