<div <?php post_class() ?>>

	<div class="page__content landing-page">

		<?php if ( ! get_field( 'lp-hide-header' ) ) : ?>

			<?php
			$classes = array( 'h-background' );
			$backgrounds = array();
			if ( get_field( 'lp-background-bottom' ) ) {
				$backgrounds[] = 'url(' . get_field( 'lp-background-bottom' ) . ')';
				$classes[] = 'landing-page__header--bottom';
			}
			if ( get_field( 'lp-background-top' ) ) {
				$backgrounds[] = 'url(' . get_field( 'lp-background-top' ) . ')';
				$classes[] = 'landing-page__header--top';
			}
			if ( get_field( 'lp-background-top' ) && get_field( 'lp-background-bottom' ) ) {
				$classes[] = 'landing-page__header--both';
			}

			$styles = array();
			if ( $backgrounds ) {
				$styles[] = 'background-image: ' . implode( ',', $backgrounds );
			}
			if ( get_field( 'lp-background-color' ) ) {
				$styles[] = 'background-color: ' . get_field( 'lp-background-color' );
			}
			?>

			<div class="landing-page__header <?php echo implode( ' ', $classes ) ?>" id="lp-header"
				<?php if ( $styles ) : ?>
					style="<?php echo implode( ';', $styles ) ?>"
				<?php endif ?>>

				<?php if ( get_field( 'lp-logo' ) ) : ?>
					<div class="landing-page__logo" <?php if ( get_field( 'lp-logo-margin' ) ) echo 'style="margin-bottom:' . get_field( 'lp-logo-margin' ) . 'px"' ?>>
						<?php if ( get_field( 'lp-logo' ) ) : ?>
							<?php echo wp_get_attachment_image( get_field( 'lp-logo' ), 'full', false, array( 'class' => 'landing-page__logo-desktop' ) ) ?>
						<?php endif ?>
					</div>
				<?php endif ?>

				<div class="l-container">
					<?php $has_title = get_field( 'lp-title' ) || get_field( 'lp-subtitle' ) ?>
					<?php $has_button = get_field( 'lp-button' ) && get_field( 'lp-link' ) ?>
					<?php if ( $has_title || $has_button ) : ?>
						<div class="landing-page__content" id="lp-content">
							<div class="l-grid">
								<?php if ( $has_title ) : ?>
									<div class="l-column l-column--tablet-<?php echo ( $has_button ) ? '2-3' : '1-1' ?>">
										<?php if ( get_field( 'lp-title' ) ) : ?>
											<div class="landing-page__title"><?php the_field( 'lp-title' ) ?></div>
										<?php endif ?>

										<?php if ( get_field( 'lp-subtitle' ) ) : ?>
											<div class="landing-page__subtitle"><?php the_field( 'lp-subtitle' ) ?></div>
										<?php endif ?>
									</div>
								<?php endif ?>

								<?php if ( $has_button ) : ?>
									<div class="l-column l-column--tablet-<?php echo ( $has_title ) ? '1-3' : '1-1' ?>">
										<div class="landing-page__button" id="lp-button">
											<a href="<?php the_field( 'lp-link' ) ?>" class="button" <?php if ( get_field( 'lp-target' ) ) echo 'target="_blank"' ?>>
												<?php the_field( 'lp-button' ) ?>
											</a>
										</div>
									</div>
								<?php endif ?>
							</div>
						</div>
					<?php endif ?>

					<?php if ( have_rows( 'lp-bubbles' ) ) : ?>
						<div class="landing-page__bubbles" id="lp-gift">
							<div class="bubbles">
								<?php while ( have_rows( 'lp-bubbles' ) ) : the_row() ?>
									<?php
									$classes = array();
									if ( get_sub_field( 'background' ) ) {
										$classes[] = 'h-background';
										$classes[] = 'h-background--' . get_sub_field( 'background' );
									}

									if ( get_sub_field( 'color' ) ) {
										$classes[] = 'h-color';
										$classes[] = 'h-color--' . get_sub_field( 'color' );
									}
									?>
									<div class="bubble <?php echo implode( ' ', $classes ) ?>">
										<?php $image = get_sub_field( 'image' ) ?>
										<div class="bubble__container <?php if ( $image ) echo 'bubble__container--with-image'?>">
											<?php if ( $image ) : ?>
												<?php echo wp_get_attachment_image( $image, 'full', false, array( 'class' => 'bubble__image' ) ) ?>
											<?php endif ?>

											<?php if ( get_sub_field( 'content' ) ) : ?>
												<div class="bubble__content <?php if ( ! $image ) echo 'bubble__content--big'?>">
													<?php the_sub_field( 'content' ) ?>
												</div>
											<?php endif ?>

										</div>
									</div>
								<?php endwhile ?>
							</div>
						</div>
					<?php endif?>

				</div>

				<?php if ( get_field( 'lp-tip-title' ) || get_field( 'lp-tip' ) ) : ?>
					<div class="landing-page__tip" id="lp-tip">
						<div class="landing-page__tip-container">
							<?php if ( get_field( 'lp-tip-title' ) ) : ?>
								<div class="landing-page__tip-title"><?php the_field( 'lp-tip-title' ) ?></div>
							<?php endif ?>
							<?php if ( get_field( 'lp-tip' ) ) : ?>
								<div class="landing-page__tip-content"><?php the_field( 'lp-tip' ) ?></div>
							<?php endif ?>
						</div>
					</div>
				<?php endif ?>

			</div>

		<?php endif ?>

		<?php if ( get_field( 'lp-baseline' ) || have_rows( 'lp-medias' ) ) : ?>
			<div class="l-section l-section--stack">
				<div class="l-container">
					<?php if ( get_field( 'lp-baseline' ) ) : ?>
						<h1 id="lp-baseline" class="landing-page__baseline"><?php the_field( 'lp-baseline' ) ?></h1>
					<?php endif ?>

					<?php if ( have_rows( 'lp-medias' ) ) : ?>
						<div class="l-grid l-grid--space landing-page__products" id="lp-products">

							<?php while ( have_rows( 'lp-medias' ) ) : the_row() ?>
								<div class="l-column l-column--tablet-<?php the_field( 'lp-medias-width' ) ?>" <?php if ( get_sub_field( 'id' ) ) echo 'id="' . get_sub_field( 'id' ) . '"' ?>>
									<div class="l-grid landing-page__product">
										<?php if ( $image = get_sub_field( 'image' ) ) : ?>
											<div class="l-column l-column--phone-1-3">
												<?php echo wp_get_attachment_image( $image, 'full', false, array( 'class' => 'landing-page__product-image' ) ) ?>
											</div>
										<?php endif ?>
										<div class="l-column l-column--phone-<?php echo ( $image ) ? '2-3' : '1-1' ?>">
											<div class="text">
												<?php if ( get_sub_field( 'title' ) ) : ?>
													<h3 class="landing-page__product-title"><?php the_sub_field( 'title' ) ?></h3>
												<?php endif ?>

												<?php the_sub_field( 'content' ) ?>
											</div>
											<a href="<?php the_sub_field( 'link' ) ?>" class="button" <?php if ( get_sub_field( 'target' ) ) echo 'target="_blank"' ?>>
												<?php the_sub_field( 'button' ) ?>
											</a>
										</div>
									</div>
								</div>
							<?php endwhile ?>
						</div>
					<?php endif ?>
				</div>
			</div>
		<?php endif ?>

		<?php if ( get_the_content() ) : ?>

			<div class="l-section l-section--stack">
				<div class="l-container">
					<div class="text">
						<?php the_content() ?>
					</div>
				</div>
			</div>

		<?php endif ?>

		<?php get_template_part( 'template-parts/layout' ) ?>

	</div>
</div>