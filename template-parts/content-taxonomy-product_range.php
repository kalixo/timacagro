<?php
global $wp_query;

// Identifiant ACF
$tax = $wp_query->queried_object;

// Couleur
$color = 'gray';
if ( get_field( 'header-color', $tax ) ) {
    $color = get_field( 'header-color', $tax );
}

$product_need = timacagro_get_product_need_filter();
$product_profile = timacagro_get_product_profile_filter();

// Liste des produits concerné par la gamme en cours de visualisation
$post_ids = get_posts( array(
    'post_type'       => 'product',
    'posts_per_page'  => -1,
    'fields'          => 'ids',
    'tax_query'       => timacagro_product_tax_query(),
) );

// Liste des taxonomies associés
$direct_need_ids = wp_get_object_terms( $post_ids, 'product_need', array( 'fields' => 'ids' ) );
$direct_profile_ids = wp_get_object_terms( $post_ids, 'product_profile', array( 'fields' => 'ids' ) );

// Liste des gammes directes
$term_ids = wp_get_object_terms( $post_ids, 'product_range', array( 'fields' => 'ids' ) );

// Liste des gammes
foreach ( $term_ids as $term_id ) {
    $term_ids = array_merge(
        $term_ids,
        get_ancestors( $term_id, $tax->taxonomy, 'taxonomy' )
    );
}
$term_ids = array_unique( $term_ids );

// Toutes les gammes enfants
$all_terms = get_terms( array(
    'taxonomy'  => $tax->taxonomy,
    'parent'    => $tax->term_id,
    'include'   => $term_ids,
) );

// Les gammes sans gamme enfant
$term_childless = get_terms( array(
    'taxonomy'  => $tax->taxonomy,
    'parent'    => $tax->term_id,
    'include'   => $term_ids,
    'childless' => true,
) );

// Les gammes avec gamme enfant
$term_hierarchical = array();
foreach ( $all_terms as $term ) {
    if ( ! in_array( $term, $term_childless)  ) {
        $term_hierarchical[] = $term;
    }
}

// les produits à afficher
$query = new WP_Query( array(
    'post_type'       => 'product',
    'posts_per_page'  => -1,
    'tax_query'		  => timacagro_product_tax_query( false ),
) );

$parent_term_title = false;

if(isset($tax->parent) && $tax->parent > 0):
    $parent_term = get_term_by('id', $tax->parent, 'product_range');
    if(isset($parent_term->parent) && $parent_term->parent > 0):
        $parent_term_title = true;
    endif;
endif;

?>
<div class="page__content taxonomy taxonomy--<?php echo $color ?>">
    <?php if ( get_field( 'taxonomy-intro', $tax ) || get_field( 'taxonomy-content', $tax ) ) : ?>

        <div class="l-section l-section--stack">
            <div class="l-container">
                <div class="taxonomy__intro">
                    <?php if(get_field('taxonomy-intro', $tax) && $parent_term_title == false): ?>
                        <span class="taxonomy__title"><?php the_field( 'taxonomy-intro', $tax ) ?></span>
                    <?php elseif($parent_term_title == true): ?>
                        <div class="gamme_logo">
                            <?php if(get_field('taxonomy-image', $tax)): ?>
                                <?php echo wp_get_attachment_image(get_field('taxonomy-image', $tax ), 'full'); ?>
                            <?php endif ?>
                            <h1 class="gamme_title product__title"><?php echo $tax->name; ?></h1>
                        </div>
                    <?php endif ?>

                    <?php if ( get_field( 'taxonomy-content', $tax ) ) : ?>
                        <div class="taxonomy__content">
                            <?php the_field( 'taxonomy-content', $tax ) ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>

        </div>

    <?php endif ?>
    <!-- / Introduction -->

    <div id="product-filter-result">

        <!-- Filter -->
        <?php if ( $query->found_posts == 0 || $query->found_posts > 1 ) : ?>
            <div class="l-section l-section--stack">
                <div class="l-container">
                    <h2 class="titre_taxonomies_premier"><?php the_field( 'premier_titre', $tax ) ?></h2>
                    <?php get_template_part( 'template-parts/filter' ) ?>
                </div>
            </div>
        <?php endif ?>
        <!-- / Filter -->

        <h2 class="titre_taxonomies_second"><?php the_field( 'second_titre', $tax ) ?></h2>

        <?php if ( empty( $post_ids ) ) : ?>

            <div class="l-section l-section--stack">
                <div class="l-container">
                    <div class="text text--center h-color--gray">
                        <?php _e( 'Aucun produit ne correspond à votre filtre', 'timacagro' ) ?>
                    </div>
                </div>
            </div>

        <?php else : ?>

            <!-- Grand Children -->
            <?php if ( ! is_wp_error( $term_hierarchical ) && is_array( $term_hierarchical ) && $term_hierarchical ) : ?>

            <div class="l-section l-section--stack">
                <div class="l-container">
                    <div class="l-grid l-grid--space l-grid--masonry">

                        <?php foreach ( $term_hierarchical as $term ) : ?>

                            <?php
                                $children = get_terms( array(
                                    'taxonomy' => $term->taxonomy,
                                    'parent' => $term->term_id,
                                    'include' => $term_ids
                                ) );
                            ?>
                            <div class="l-column l-column--tablet-1-2">
                                <div class="taxonomy__parent">
                                    <a href="<?php echo esc_url( timacagro_filter_link( get_term_link( $term ) ) ) ?>" class="taxonomy__parent-title">
                                        <?php echo $term->name ?>
                                    </a>

                                    <div class="taxonomy__children">

                                        <?php foreach ( $children as $child ) : ?>

                                            <div class="taxonomy-item">

                                                <div class="taxonomy-item__image">

                                                    <?php if ( get_field( 'taxonomy-image', $child ) ) : ?>
                                                        <?php echo wp_get_attachment_image( get_field( 'taxonomy-image', $child ), 'full' ) ?>
                                                    <?php endif ?>

                                                </div>

                                                <div class="taxonomy-item__body">
                                                    <div class="text">
                                                        <h3 class="taxonomy-item__title"><?php echo $child->name ?></h3>

                                                        <?php if ( $child->description ) : ?>
                                                            <p class="taxonomy-item__description">
                                                                <?php echo $child->description ?>
                                                            </p>
                                                        <?php endif ?>

                                                        <a href="<?php echo esc_url( timacagro_filter_link( get_term_link( $child ) ) ) ?>" class="taxonomy-item__link">
                                                            <?php _e( 'Voir les produits', 'timacagro' ) ?>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endforeach ?>

                                    </div>
                                </div>
                            </div>

                        <?php endforeach ?>

                    </div>

                </div>
            </div>

            <?php endif ?>
            <!-- / Grand Children -->


            <!-- Direct Children -->
            <?php if ( ! is_wp_error( $term_childless ) && is_array( $term_childless ) && $term_childless ) : ?>

            <div class="l-section l-section--stack">
                <div class="l-container">
                    <div class="l-grid l-grid--space l-grid--masonry">

                        <?php foreach ( $term_childless as $term ) : ?>

                            <div class="l-column l-column--tablet-1-2">

                                <div class="taxonomy-item">

                                    <div class="taxonomy-item__image">

                                        <?php if ( get_field( 'taxonomy-image', $term ) ) : ?>
                                            <?php echo wp_get_attachment_image( get_field( 'taxonomy-image', $term ), 'full' ) ?>
                                        <?php endif ?>

                                    </div>

                                    <div class="taxonomy-item__body">
                                        <div class="text">
                                            <h3 class="taxonomy-item__title"><?php echo $term->name ?></h3>

                                            <?php if ( $term->description ) : ?>
                                                <p class="taxonomy-item__description">
                                                    <?php echo $term->description ?>
                                                </p>
                                            <?php endif ?>

                                            <a href="<?php echo esc_url( timacagro_filter_link( get_term_link( $term ) ) ) ?>" class="taxonomy-item__link">
                                                <?php _e( 'Voir les produits', 'timacagro' ) ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        <?php endforeach ?>

                    </div>
                </div>
            </div>

            <?php endif ?>
            <!-- / Direct Children -->


            <?php if ( $query->have_posts() ) : ?>

                <div class="l-section l-section--stack">

                    <div class="l-container">

                        <div class="cards cards-center-2">

                            <?php while ( $query->have_posts() ) : $query->the_post() ?>

                                <?php get_template_part( 'template-parts/loop', 'product' ) ?>

                            <?php endwhile ; wp_reset_postdata() ?>

                        </div>

                    </div>

                </div>

            <?php endif  ?>

        <?php endif ?>

    </div>


    <div class="l-section l-section--space h-background h-background--gray h-background--effect">
        <div class="l-container">
            <div class="text text--lead text--center">
                <h2><?php the_field( 'footer-range-title', 'option' ) ?></h2>
                <?php the_field( 'footer-range-content', 'option' ) ?>
                <a href="<?php echo esc_url( timacagro_context_url( get_field( 'footer-range-link', 'option' ) ) ) ?>" class="button js-popin mfp-ajax"
                    onclick="ga('send','event','CTA-Etre-contacte','Clic','CTA-Gamme',1);">
                    <?php the_field( 'footer-range-button', 'option' ) ?>
                    <span class="icon icon--phone"></span>
                </a>
            </div>
        </div>
    </div>

</div>
