<?php
$classes = array( 'text' );

if ( get_sub_field( 'align' ) ) {
	$classes[] = 'text--' . get_sub_field( 'align' );
}
?>

<div class="<?php echo implode( ' ', $classes ) ?>">
	<?php the_sub_field( 'content' ) ?>
</div>