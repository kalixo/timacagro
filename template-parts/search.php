<?php
$product_need = timacagro_get_product_need_filter();
$product_profile = timacagro_get_product_profile_filter();

// Liste des produits concernés par la vue en cours
$post_ids = get_posts( array(
	'post_type'       => 'product',
	'posts_per_page'  => -1,
	'fields'          => 'ids',
	'tax_query' 	  => timacagro_product_tax_query()
) );

// Liste des profiles
$profiles = wp_get_object_terms( $post_ids, 'product_profile', array( 'fields' => 'ids' ) );
$profiles = get_terms( array( 'taxonomy' => 'product_profile', 'include' => $profiles, 'fields' => 'ids' ) );

// Si un besoin est sélectionné, on ne récupère que les profiles associés à ce besoin
if ( $product_need ) {
	$selected_need = get_term_by( 'slug', $product_need, 'product_need' );
	if ( $related_profiles = get_field( 'related-taxonomies', $selected_need ) ) {
		$profiles = array_intersect( $profiles, $related_profiles );
	}
}

// Liste des besoins
$direct_needs = wp_get_object_terms( $post_ids, 'product_need', array( 'fields' => 'ids' ) );

// Si un profile est sélectionné, on ne récupère que les besoins associés à ce profile
if ( $product_profile ) {
	$selected_profile = get_term_by( 'slug', $product_profile, 'product_profile' );
	if ( $related_needs = get_field( 'related-taxonomies', $selected_profile ) ) {
		$direct_needs = array_intersect( $direct_needs, $related_needs );
	}
}

$needs = $direct_needs;
foreach ( $direct_needs as $need ) {
	$needs = array_merge(
		$needs,
		get_ancestors( $need, 'product_need', 'taxonomy' )
	);
}
$needs = array_unique( $needs );

$parent_needs = get_terms( array( 'taxonomy' => 'product_need', 'parent' => 0, 'include' => $needs ) );

// Liste des gammes directes
$direct_ranges = wp_get_object_terms( $post_ids, 'product_range', array( 'fields' => 'ids' ) );
$ranges = $direct_ranges;
foreach ( $direct_ranges as $range ) {
	$ranges = array_merge(
		$ranges,
		get_ancestors( $range, 'product_range', 'taxonomy' )
	);
}
$ranges = array_unique( $ranges );

$parent_ranges = get_terms( array( 'taxonomy' => 'product_range', 'parent' => 0, 'include' => $ranges ) );

?>
<form action="<?php echo get_post_type_archive_link( 'product' ) ?>" class="search js-ajax-search" method="get">
	<label class="search__label" for=""><?php _e( 'Rechercher un produit', 'timacagro' ) ?></label>
	<div class="search__fields">
		<select name="product_profile" class="search__select">
			<option value="" class="option option--placeholder"><?php _e( 'Tous les profils', 'timacagro' ) ?></option>
			<?php foreach ( $profiles as $profile_id ) : $profile = get_term( $profile_id, 'product_profile' ) ?>
				<option value="<?php echo $profile->slug ?>" <?php if ( $profile->slug == $product_profile ) echo 'selected="selected"' ?>><?php echo $profile->name ?></option>
			<?php endforeach ?>
		</select>

		<select name="product_need" class="search__select">
			<option value="" class="option option--placeholder"><?php _e( 'Tous les besoins', 'timacagro' ) ?></option>
			<?php foreach ( $parent_needs as $parent ) {

				// Les enfants sélectionnables du besoin parcouru
				$children = get_terms( array( 'taxonomy' => 'product_need', 'parent' => $parent->term_id, 'include' => $needs ) );

				// Il a des enfants
				if ( ! is_wp_error( $children ) && ( $children || in_array( $parent->term_id, $needs ) ) ) {
					// On affiche le parent
					$selected = ( $parent->slug == $product_need ) ? 'selected="selected"' : '';
					$disabled = ( ! in_array( $parent->term_id, $direct_needs ) ) ? 'disabled' : '';

					echo sprintf(
						'<option value="%s" class="option option--parent" %s %s>%s</option>',
						$parent->slug,
						$disabled,
						$selected,
						$parent->name
					) ;

					foreach ( $children as $child ) {
						// Les enfants sélectionnables du besoin parcouru
						$grand_children = get_terms( array( 'taxonomy' => 'product_need', 'parent' => $child->term_id, 'include' => $needs ) );

						// Il a des enfants
						if ( ! is_wp_error( $grand_children ) && ( $grand_children || in_array( $child->term_id, $needs ) ) ) {
							// On affiche le parent
							$selected = ( $child->slug == $product_need ) ? 'selected="selected"' : '';
							$disabled = ( ! in_array( $child->term_id, $direct_needs ) ) ? 'disabled' : '';

							echo sprintf(
								'<option value="%s" class="option option--parent" %s %s>&ensp;%s</option>',
								$child->slug,
								$disabled,
								$selected,
								$child->name
							) ;
							foreach ( $grand_children as $grand_child ) {
								$selected = ( $grand_child->slug == $product_need ) ? 'selected="selected"' : '';

								echo sprintf(
									'<option value="%s" class="option option--child" %s>&ensp;&ensp;%s</option>',
									$grand_child->slug,
									$selected,
									$grand_child->name
								) ;
							}
						}
					}
				}
			} ?>
		</select>

		<select name="product_range" class="search__select">
			<option value="" class="option option--placeholder"><?php _e( 'Toutes les gammes', 'timacagro' ) ?></option>
			<?php foreach ( $parent_ranges as $parent ) {

				// Les enfants sélectionnables du besoin parcouru
				$children = get_terms( array( 'taxonomy' => 'product_range', 'parent' => $parent->term_id, 'include' => $ranges ) );

				// Il a des enfants
				if ( ! is_wp_error( $children ) && ( $children || in_array( $parent->term_id, $ranges ) ) ) {
					// On affiche le parent
					$selected = ( $parent->slug == get_query_var( 'product_range' ) ) ? 'selected="selected"' : '';

					echo sprintf(
						'<option value="%s" class="option option--parent" disabled %s>%s</option>',
						$parent->slug,
						$selected,
						$parent->name
					) ;

					foreach ( $children as $child ) {
						// Les enfants sélectionnables du besoin parcouru
						$grand_children = get_terms( array( 'taxonomy' => 'product_range', 'parent' => $child->term_id, 'include' => $ranges ) );

						// Il a des enfants
						if ( ! is_wp_error( $grand_children ) && ( $grand_children || in_array( $child->term_id, $ranges ) ) ) {
							// On affiche le parent
							$selected = ( $child->slug == get_query_var( 'product_range' ) ) ? 'selected="selected"' : '';

							echo sprintf(
								'<option value="%s" class="option option--parent" disabled %s>&ensp;%s</option>',
								$child->slug,
								$selected,
								$child->name
							) ;
							foreach ( $grand_children as $grand_child ) {
								$selected = ( $grand_child->slug == get_query_var( 'product_range' ) ) ? 'selected="selected"' : '';

								echo sprintf(
									'<option value="%s" class="option option--child" %s>&ensp;&ensp;%s</option>',
									$grand_child->slug,
									$selected,
									$grand_child->name
								) ;
							}
						}
					}
				}
			} ?>
		</select>
		<input type="submit" value="<?php _e( 'Rechercher', 'timacagro' ) ?>" class="button search__button" onclick="ga('send','event','Bouton-rechercher','Clic','Rechercher',1);">
	</div>
</form>
