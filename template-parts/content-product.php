<?php
// La gamme du produit
$product_range = null;

$terms = get_the_terms( get_the_ID(), 'product_range' );
if ( $terms && ! is_wp_error( $terms ) ) {
    $product_range = array_shift( $terms );
}

// Couleur du produit
$color = 'gray';
if ( get_field( 'header-color', $product_range ) ) {
    $color = get_field( 'header-color', $product_range );
}
if ( get_field( 'header-color' ) ) {
    $color = get_field( 'header-color' );
}

// Intitulé des titres
$product_issues_title = get_field( 'product-issues-title' ) ? get_field( 'product-issues-title' ) : get_field( 'product-issues-title', 'options' );
$product_profiles_title = get_field( 'product-profiles-title' ) ? get_field( 'product-profiles-title' ) : get_field( 'product-profiles-title', 'options' );
$product_profit_title = get_field( 'product-profit-title' ) ? get_field( 'product-profit-title' ) : get_field( 'product-profit-title', 'options' );

// Intitulé des boutons
$product_document_button = get_field( 'product-document-button' ) ? get_field( 'product-document-button' ) : get_field( 'product-document-button', 'options' );
$product_diagnotic_button = get_field( 'product-diagnostic-button' ) ? get_field( 'product-diagnostic-button' ) : get_field( 'product-diagnostic-button', 'options' );
$product_contact_button = get_field( 'product-contact-button' ) ? get_field( 'product-contact-button' ) : get_field( 'product-contact-button', 'options' );

// Fiche de sécurité
$product_security_text = get_field( 'product-security-text' ) ? get_field( 'product-security-text' ) : get_field( 'product-security-text', 'options' );
$product_security_link = get_field( 'product-security-link' ) ? get_field( 'product-security-link' ) : get_field( 'product-security-link', 'options' );

?>

<div <?php post_class( 'product product--' . $color ) ?>>
    <div class="page__content">

        <!-- Slider -->
        <?php if ( have_rows( 'product-slider' ) ) : ?>
            <div class="slider product__slider">
                <ul class="slider__list js-slider">

                    <?php while ( have_rows( 'product-slider' ) ) : the_row() ?>

                        <?php
                            // Le slide ouvre une popin
                            $is_popin = 'popin' == get_sub_field( 'interaction' );

                            // le slide ouvre un lien
                            $is_link = 'link' == get_sub_field( 'interaction' ) || $is_popin;

                            // le slide a un bouton
                            $has_button = 'button' == get_sub_field( 'interaction' );

                            // La slide a du contenu a afficher
                            $has_content = get_sub_field( 'title' ) || get_sub_field( 'content' ) || $has_button;

                            $target = ( get_sub_field( 'target' ) ) ? 'target="_blank"' : '';

                            $styles = array( 'height: 300px' );

                            // Image d'arrière plan
                            if ( get_sub_field( 'image' ) ) {
                                $styles[] = 'background-image: url(' . wp_get_attachment_image_url( get_sub_field( 'image' ), 'full' ) . ')';
                            }
                        ?>
                        <li class="slider__slide slider__slide--fixed" <?php if ( $styles ) echo 'style="' . implode( ';', $styles ) . '"' ?>>

                            <?php if ( $is_link ) : ?>
                                <a class="slider__inner slider__inner--link <?php if ( $is_popin ) echo 'js-popin mfp-iframe' ?>" href="<?php the_sub_field( 'link' ) ?>" <?php echo $target ?>>
                            <?php else : ?>
                                <div class="slider__inner">
                            <?php endif ?>

                                <?php if ( $has_content ) : ?>

                                    <div class="slider__content">
                                        <div class="l-container">

                                            <div class="h-media">
                                                <div class="h-media__body">
                                                    <div class="slider__title"><?php the_sub_field( 'title' ) ?></div>
                                                    <?php the_sub_field( 'content' ) ?>
                                                </div>

                                                <?php if ( $has_button ) : ?>
                                                    <div class="h-media__object h-media__object--middle h-media__object--right">
                                                        <a href="<?php the_sub_field( 'link' ) ?>" class="button" <?php echo $target ?>>
                                                            <?php the_sub_field( 'button' ) ?>
                                                        </a>
                                                    </div>
                                                <?php endif ?>
                                            </div>

                                        </div>
                                    </div>

                                <?php endif ?>

                            <?php if ( $is_link ) : ?>
                                </a>
                            <?php else : ?>
                                </div>
                            <?php endif ?>

                        </li>

                    <?php endwhile ?>

                </ul>
            </div>

        <?php endif ?>

        <!-- / Slider -->

        <div class="l-section l-section--space product__content">
            <div class="l-container">
                <div class="l-grid l-grid--space">
                    <div class="l-column l-column--tablet-2-3 product__introduction">
                        <div class="gamme__logo">
                            <?php
                                if(isset($product_range)):
                                    if(get_field('taxonomy-image', $product_range)):
                                        echo wp_get_attachment_image(get_field('taxonomy-image', $product_range ), 'full');
                                    endif;
                                endif;
                            ?>
                        </div>
                        <div class="text product__intro">
                            <h1><?php the_title(); ?></h1>

                            <?php if ( get_field( 'header-subtitle' ) ) : ?>
                                <h2><?php the_field( 'header-subtitle' ); ?></h2>
                            <?php endif ?>

                            <?php if ( get_field( 'product-introduction' ) ) : ?>
                                <?php the_field( 'product-introduction' ); ?>
                            <?php endif ?>
                        </div>

                        <div class="gamme__logo__mobile">
                            <?php
                                if(isset($product_range)):
                                    if(get_field('taxonomy-image', $product_range)):
                                        echo wp_get_attachment_image(get_field('taxonomy-image', $product_range ), 'full');
                                    endif;
                                endif;
                            ?>
                        </div>
                        <div class="text product__intro__mobile">
                            <span class="product__title"><?php the_title(); ?></span>

                            <?php if ( get_field( 'header-subtitle' ) ) : ?>
                                <span class="product__sub"><?php the_field( 'header-subtitle' ); ?></span>
                            <?php endif ?>

                            <?php if ( get_field( 'product-introduction' ) ) : ?>
                                <?php the_field( 'product-introduction' ); ?>
                            <?php endif ?>
                        </div>
                    </div>

                    <div class="l-column l-column--tablet-1-3 product__expert__container">
                        <?php if(have_rows('liens_services', 'options')) : ?>
                            <div class="text product__expert">
                                <h3><?php _e('Nos experts', 'timac-agro'); ?><br /><?php _e('à votre service', 'timac-agro'); ?></h3>
                                <div class="product__services">
                                    <?php $dark = false; ?>
                                    <?php while ( have_rows( 'liens_services', 'options' ) ) : the_row() ?>
                                        <?php
                                            $link = get_sub_field( 'link' );
                                            if ( get_sub_field( 'popin' ) ) {
                                                $link = timacagro_context_url( $link );
                                            }
                                        ?>
                                        <a href="<?php echo esc_url( $link ) ?>" class="<?php if ( get_sub_field( 'popin' ) ) echo 'js-popin mfp-ajax' ?> kxowave<?php echo $ii++ ; ?><?php if($dark == true): echo ' dark'; endif; ?>" <?php if ( get_sub_field( 'popin' ) ) echo "onclick=\"ga('send','event','CTA-RDV-expert','Clic','CTA',1);\"" ?> >
                                            <?php if ( get_sub_field( 'icon' ) ) : ?>
                                                <span class="aside__icon">
                                                    <span class="icon icon--<?php the_sub_field( 'icon' ) ?>"></span>
                                                </span>
                                            <?php endif ?>
                                            <span class="aside__text">
                                                <?php the_sub_field( 'text' ) ?>
                                            </span>
                                        </a>
                                        <?php
                                            if($dark == false):
                                                $dark = true;
                                            else:
                                                $dark = false;
                                            endif;
                                        ?>
                                    <?php endwhile ?>

                                    <?php //if(get_field('product-document') || get_field('product-diagnostic') || get_field('product-contact')): ?>

                                        <!-- Documentation -->
                                        <?php //if ( get_field( 'product-document' ) ) : ?>
                                            <!-- <a href="<?php //the_field( 'product-document-file' ) ?>" class="<?php //if($dark == true): echo ' dark'; endif; ?>">
                                                <span class="aside__icon"><span class="icon icon--download"></span></span>
                                                <span class="aside__text"><?php //echo $product_document_button ?></span>
                                            </a> -->
                                            <?php
                                                // if($dark == false):
                                                //     $dark = true;
                                                // else:
                                                //     $dark = false;
                                                // endif;
                                            ?>
                                        <?php //endif ?>
                                        <!-- / Documentation -->

                                        <!-- Diagnostic -->
                                        <?php //if ( get_field( 'product-diagnostic' ) ) : ?>
                                            <!-- <a href="<?php //echo esc_url( timacagro_context_url( get_field( 'product-diagnostic-link', 'option' ) ) ) ?>" class="<?php //if($dark == true): echo ' dark'; endif; ?> js-popin mfp-ajax" onclick="ga('send','event','CTA-Demande-diagnostic','Clic','CTA-produit',1);">
                                                <span class="aside__icon"><span class="icon icon--diagnosis"></span></span>
                                                <span class="aside__text"><?php //echo $product_diagnotic_button ?></span>
                                            </a> -->
                                            <?php
                                                // if($dark == false):
                                                //     $dark = true;
                                                // else:
                                                //     $dark = false;
                                                // endif;
                                            ?>
                                        <?php //endif ?>
                                        <!-- / Diagnostic -->

                                    <?php //endif ?>

                                </div>
                            </div>
                        <?php endif; ?>
                    </div>

                </div>
            </div>
        </div>

        <!-- Content -->
        <div class="l-section l-section--space product__content">
            <div class="l-container">
                <div class="l-grid l-grid--space">

                    <!-- Visuel -->
                    <?php if ( has_post_thumbnail() ) : ?>
                        <div class="l-column l-column--tablet-1-3">
                            <?php the_post_thumbnail( 'full', array('class' => 'product__image') ) ?>

                            <!-- Conditionnement -->
                            <?php if ( get_field( 'product-packaging' ) ) : ?>
                                <div class="text conditionnement">
                                    <h3 class="product__subtitle"><?php the_field( 'product-packaging-title', 'options' ) ?></h3>
                                    <?php the_field( 'product-packaging' ) ?>
                                </div>
                            <?php endif ?>
                            <!-- / Conditionnement -->
                        </div>
                    <?php endif ?>
                    <!-- / Visuel -->

                    <div class="l-column l-column--tablet-2-3">

                        <!-- Sections -->
                        <div class="l-grid l-grid--space">
                            <div class="l-column l-column--tablet-1-2">

                                <?php if ( $terms = get_the_terms( get_the_ID(), 'product_profile' ) ) : ?>
                                    <div class="text product__section product__section_block grey_background">
                                        <h3 class="product__section-title product__subtitle"><?php echo $product_profiles_title ?></h3>
                                        <ul>
                                            <?php foreach ( $terms as $term ) : ?>
                                                <li><?php echo $term->name ?></li>
                                            <?php endforeach ?>
                                        </ul>
                                    </div>
                                <?php endif ?>
                                <?php if ( get_field( 'product-issues' ) ) : ?>
                                    <div class="text product__section product__section_block">
                                        <h3 class="product__section-title product__subtitle"><?php echo $product_issues_title ?></h3>
                                        <div>
                                            <?php the_field( 'product-issues-content' ) ?>
                                        </div>
                                    </div>
                                <?php endif ?>

                            </div>

                            <div class="l-column l-column--tablet-1-2">
                                                                <?php if ( get_field( 'product-profit' ) ) : ?>
                                    <div class="text product__section product__section_block">
                                        <h3 class="product__section-title product__subtitle"><?php echo $product_profit_title ?></h3>
                                        <div>
                                            <?php the_field( 'product-profit-content' ) ?>
                                        </div>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                        <!-- / Sections -->
                        <!-- Sections -->
                        <div class="l-grid--head">
                              <!-- Description du produit -->
                                    <?php if ( get_field( 'product-description' ) || get_field( 'product-bio' ) || ! get_field( 'product-security-hide' ) ) : ?>
                                        <div class="l-column l-column--tablet-1-2">
                                            <div class="text">
                                                <?php if ( get_field( 'product-description' ) ) : ?>
                                                    <h3 class="product__subtitle"><?php the_field( 'product-description-title', 'options' ) ?></h3>
                                                    <?php the_field( 'product-description' ) ?>
                                                <?php endif ?>

                                                <?php if ( get_field( 'product-bio' ) ) : ?>
                                                    <p class="pastille">
                                                        <span class="pastille__tag"><?php _e( 'BIO', 'timacagro' ) ?></span>
                                                        <span class="pastille__label">
                                                            <?php the_field( 'product-bio-label' ) ?>
                                                        </span>
                                                    </p>
                                                <?php endif ?>

                                                <?php if ( ! get_field( 'product-security-hide' ) ) : ?>
                                                    <a href="<?php echo $product_security_link ?>" class="pastille" target="_blank">
                                                        <span class="pastille__tag pastille__tag--security">
                                                            <span class="icon icon--download"></span>
                                                        </span>
                                                        <span class="pastille__label">
                                                            <?php echo $product_security_text ?>
                                                        </span>
                                                    </a>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                    <!-- / Description du produit -->

                                    <!-- More description -->
                                    <?php if(get_field('more_description')): ?>
                                        <div class="l-column l-column--tablet-1-2 more_description">
                                            <div class="text">
                                                <?php the_field('more_description'); ?>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                    <!-- / More description -->
                        </div>
                        <!-- / Sections -->
                    </div>
                </div>

            </div>

        </div>
        <!-- / Content -->

        <!-- Tabulation -->
        <div class="l-section">
            <div class="tabs">
                <div class="tabs__container">
                    <div class="tabs__content tabs__content--active" id="tab-description">
                        <div class="l-section l-section--small-stack">
                            <div class="l-container">
                                <div class="l-grid">

                                    <!-- Conditionnement
                                    <?php if ( get_field( 'product-packaging' ) ) : ?>
                                        <div class="l-column l-column--tablet-1-3">
                                            <div class="text">&nbsp;</div>
                                        </div>
                                    <?php endif ?>
                                    <!-- / Conditionnement


                                </div>
                            </div>
                        </div>
                    </div>

                    <?php while ( have_rows( 'product-tabs' ) ) : the_row() ?>
                        <div class="tabs__content" id="tab-other-<?php echo get_row_index() ?>">
                            <div class="l-section l-section--small-stack">
                                <div class="l-container">
                                    <div class="text">
                                        <?php the_sub_field( 'content' ) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile ?>

                </div>
            </div>
        </div>
        <!-- / Tabulation -->

        <!-- Offre promotionnelle -->
        <?php if ( get_field( 'product-offer' ) ) : ?>
            <div class="product l-section l-section--stack">
                <div class="l-container">
                    <div class="offer">
                        <?php if ( get_field( 'product-offer-image' ) ) : ?>
                            <div class="offer__image" style="background-image: url(<?php echo wp_get_attachment_image_url( get_field( 'product-offer-image' ), 'slider' ) ?>)">
                            </div>
                        <?php endif ?>
                        <div class="offer__content">
                            <div class="text">
                                <h3 class="offer__title product__subtitle"><?php the_field( 'product-offer-title' ) ?></h3>
                                <?php the_field( 'product-offer-content' ) ?>
                            </div>
                            <?php if ( get_field( 'product-offer-link' ) ) : ?>
                                <div class="offer__button">
                                    <a href="<?php the_field( 'product-offer-link' ) ?>" class="button" <?php if ( get_field( 'product-offer-target' ) ) echo 'target="_blank"' ?>>
                                        <?php the_field( 'product-offer-button' ) ?>
                                    </a>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif ?>
        <!-- / Offre promotionnelle -->

        <!-- Vidéos -->
        <?php if(get_field('videos')): ?>
            <div class="l-section">
                <div class="l-container">
                    <hr>
                </div>
            </div>

            <div class="l-section l-section--space h-background h-background--white h-background--effect video-section">
                <h2 class="product__related-title"><?php _e('Découvrir le produit ', 'timac-agro'); ?><?php _e('en vidéo', 'timac-agro'); ?></h2>
                <div class="l-container video-wrapper">
                    <?php while(have_rows('videos')): ?>
                      <?php $rows = get_field('videos'); ?>
                      <?php $row_count = count($rows); ?>
                      <?php if($row_count > 1): ?>
                        <?php $classes = 'l-column--tablet-2-4 video'; ?>
                        <?php $styles = ''; ?>
                      <?php else: ?>
                        <?php $classes = 'video'; ?>
                        <?php $styles = 'style="text-align:center;float:inherit;width:100%"'; ?>
                      <?php endif; ?>
                        <?php the_row(); ?>
                        <div class="l-column <?php echo $classes; ?>" <?php echo $styles; ?>>
                            <div class="embed-container-2">
                                <div class="video-container">
                                <?php the_sub_field('video'); ?></div>
                            </div>
                            <h3><?php the_sub_field('titre'); ?></h3>
                            <p><?php the_sub_field('description'); ?></p>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif ?>
        <!-- / Vidéos -->

        <!-- Produits associés -->
        <?php if ( $related = get_field( 'product-related' ) ) : ?>
            <div class="l-section">
                <div class="l-container">
                    <hr>
                </div>
            </div>

            <div class="l-section l-section--space h-background h-background--white h-background--effect">
                <h2 class="product__related-title"><?php the_field( 'product-related-title' ) ?></h2>

                <div class="cards cards--gray card--white cards-center-2">

                    <?php foreach ( $related as $post ) : ?>
                        <?php setup_postdata( $post ); ?>

                        <?php get_template_part( 'template-parts/loop', 'product' ) ?>

                    <?php endforeach ; wp_reset_postdata() ?>

                </div>
            </div>
        <?php endif ?>
        <!-- / Produits associés -->



    </div>
</div>
</div>

        <!-- Être contacté -->
        <div class="l-section l-section--space h-background h-background--gray h-background--effect">
            <div class="l-container">
                <div class="text text--lead text--center">
                    <h2><?php the_field( 'footer-range-title', 'option' ) ?></h2>
                    <?php the_field( 'footer-range-content', 'option' ) ?>
                    <a href="<?php echo esc_url( timacagro_context_url( get_field( 'footer-range-link', 'option' ) ) ) ?>" class="button js-popin mfp-ajax"
                        onclick="ga('send','event','CTA-Etre-contacte','Clic','CTA-Gamme',1);">
                        <?php the_field( 'footer-range-button', 'option' ) ?>
                        <span class="icon icon--phone"></span>
                    </a>
                </div>
            </div>
        </div>
        <!-- / Être contacté -->
