<?php
global $wp_query;

// Identifiant ACF
$tax = $wp_query->queried_object;

// Couleur
$color = 'gray';
if ( get_field( 'header-color', $tax ) ) {
    $color = get_field( 'header-color', $tax );
}
?>
<div class="page__content taxonomy taxonomy--<?php echo $color ?>">

    <?php if ( get_field( 'taxonomy-intro', $tax ) || get_field( 'taxonomy-content', $tax ) ) : ?>

        <div class="l-section l-section--stack">
            <div class="l-container">
                <div class="taxonomy__intro">
                    <?php if ( get_field( 'taxonomy-intro', $tax ) ) : ?>
                        <h1 class="taxonomy__title"><?php the_field( 'taxonomy-intro', $tax ) ?></h1>
                    <?php endif ?>

                    <?php if ( get_field( 'taxonomy-content', $tax ) ) : ?>
                        <div class="taxonomy__content">
                            <?php the_field( 'taxonomy-content', $tax ) ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>

        </div>

    <?php endif ?>

</div>
