<div <?php post_class() ?>>
	<div class="page__content">
		<div class="l-section l-section--stack">
			<div class="l-container">

				<div class="text text--center">
					<h2><?php _e( 'Sélectionner un ou plusieurs critères pour affiner votre recherche', 'timacagro' ) ?></h2>
				</div>

				<?php get_template_part( 'template-parts/search' ) ?>

			</div>
		</div>
	</div>
</div>