<div class="card card__product card__product_v2">
<a href="<?php echo esc_url( timacagro_filter_link( get_permalink() ) ) ?>">
  <div class="card__product_v2_img"
    <?php if(has_post_thumbnail()): ?>
      style="background-image: url('<?php the_post_thumbnail_url('card'); ?>')"
    <?php endif; ?>
  >
  </div></a>
  <div class="card__content">
   <h3 class="card__title"><?php the_title(); ?></h3>
    <div class="card__subtitle">
      <?php if(get_field('product-introduction')): ?>
        <?php $resume = get_field('product-introduction'); ?>
        <?php echo $resume; ?>
      <?php endif; ?>
    </div>
    <div class="card__button">
      <a href="<?php echo esc_url( timacagro_filter_link( get_permalink() ) ) ?>" class="button button--fullwidth" title="<?php _e( 'Voir la fiche produit', 'timacagro' ) ?>">
        <?php _e( 'Voir la fiche produit', 'timacagro' ) ?>
      </a>
    </div>
  </div>
</div>
