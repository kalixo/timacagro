<div <?php post_class() ?>>

	<div class="page__content">

		<div class="l-section l-section--stack">
			<div class="l-container">
				<div class="text">
					<?php the_field( '404-content', 'option' ) ?>
				</div>
			</div>
		</div>

	</div>
</div>