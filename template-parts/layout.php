<?php if ( have_rows( 'layout' ) ) : ?>

	<div class="layout">

		<?php while ( have_rows( 'layout' ) ) : the_row() ?>

			<?php get_template_part( 'template-parts/layout', 'row' ) ?>

		<?php endwhile ?>

	</div>

<?php endif ?>
