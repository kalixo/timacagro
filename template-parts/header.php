<?php

// Couleur du produit
$color = 'gray';
if ( get_field( 'header-color' ) ) {
	$color = get_field( 'header-color' );
}
$color_class = 'h-background--' . $color;

if ( ! get_field( 'header-hide' ) ) : ?>

	<div class="page__header header h-background <?php echo $color_class ?>">
		<div class="l-container">
			<div class="header__top">

				<?php if ( function_exists( 'bcn_display' ) ) : ?>
					<!-- Fil d'ariane -->
					<div class="header__breadcrumb breadcrumb" typeof="BreadcrumbList" vocab="http://schema.org/">
						<?php bcn_display() ?>
					</div>
					<!-- / Fil d'ariane -->
				<?php endif ?>

			</div>

			<div class="header__content">

				<?php if ( get_field( 'header-icon' ) ) : ?>
					<?php echo wp_get_attachment_image( get_field( 'header-icon' ), 'full', false, array( 'class' => 'header__icon' ) ) ?>
				<?php endif ?>

				<!-- Titre -->
				<h1 class="header__title page__title"><?php the_title() ?></h1>

				<!-- Sous titre -->
				<?php if ( get_field( 'header-subtitle' ) ) : ?>
					<p class="header__subtitle"><?php the_field( 'header-subtitle' ) ?></p>
				<?php endif ?>
			</div>
		</div>
	</div>

<?php endif ?>
