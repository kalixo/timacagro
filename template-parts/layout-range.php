<?php $ranges = get_sub_field( 'ranges' ) ?>

<?php foreach ( $ranges as $term ) : ?>
  <?php $univers_nos_produits = get_field('univers_nos_produits', $term); ?>
    <a href="<?php echo esc_url( timacagro_filter_link( get_term_link( $term ) ) ) ?>"  >
        <div class="card card--expanding produits"
            <?php if ( get_field( 'taxonomy-image', $term ) ) : ?>
                style="background-image: url(<?php echo wp_get_attachment_image_url( get_field( 'taxonomy-image', $term ), 'card' ); ?>)"
            <?php endif ?>>
            <div class="card__content nosproduits_content">
                <h3 class="card__title">
                  <?php
                    if($univers_nos_produits['univers-titre']):
                      echo $univers_nos_produits['univers-titre'];
                    else:
                      echo $term->name;
                    endif;
                  ?>
                </h3>
                <p class="card__subtitle nosproduits_subtitle">
                  <?php
                    if($univers_nos_produits['univers-sous_titre']):
                      echo $univers_nos_produits['univers-sous_titre'];
                    else:
                      the_field('taxonomy-intro', $term);
                    endif;
                  ?>
                </p>
            </div>
            <div class="card__icone">
              <?php
                if($univers_nos_produits['univers-icone']):
                  echo '<img src="'.wp_get_attachment_image_url($univers_nos_produits['univers-icone'], 'card').'" srcset="'.wp_get_attachment_image_url($univers_nos_produits['univers-icone'], 'card').'" class="icn_sol" />';
                else:
                  if(get_field('header-icon', $term)):
                    echo '<img src="'.wp_get_attachment_image_url(get_field('header-icon', $term), 'card').'" srcset="'.wp_get_attachment_image_url(get_field('header-icon', $term), 'card').'" class="icn_sol" />';
                  endif;
                endif;
              ?>
            </div>
        </div>
    </a>
    <div class="card__contentcard">
        <?php if ( $term->description ) : ?>
            <div class="taxonomy-item__description">
                <?php echo $term->description ?>
            </div>
        <?php endif ?>
        <div class="card__button">
            <a href="<?php echo esc_url( timacagro_filter_link( get_term_link( $term ) ) ) ?>" class="button button--fullwidth <?php echo $term->slug; ?>">
                <?php _e( 'Découvrir notre gamme ', 'timacagro' ) ?><?php echo $term->slug ?>
            </a>
        </div>
    </div>
<?php endforeach ?>
