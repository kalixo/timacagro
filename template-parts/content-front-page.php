<div <?php post_class( 'page' ) ?>>

    <div class="page__content">

        <?php if ( have_rows( 'sliders' ) ) : ?>
            <div class="search-slider">
                <div class="search-slider__slider slider slider--home">
                    <ul class="slider__list js-slider" id="home-slider" data-target="#home-tab-slider">
                        <?php while ( have_rows( 'sliders' ) ) : the_row() ?>
                            <li class="slider__slide"
                                <?php if ( get_sub_field( 'image' ) ) : ?>
                                style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field( 'image' ), 'slider' ) ?>)"
                                <?php endif ?>>
                                <div class="slider__inner">
                                    <div class="slider__splash">
                                        <div class="l-container">
                                            <div class="slider__baseline">
                                                <?php the_sub_field( 'content' ) ?>
                                            </div>

                                            <?php if ( get_sub_field( 'button' ) && get_sub_field( 'link' ) ) : ?>
                                                <div class="slider__button">
                                                    <a href="<?php the_sub_field( 'link' ) ?>" class="button" <?php if ( get_sub_field( 'target' ) ) echo 'target="_blank"' ?>>
                                                        <?php the_sub_field( 'button' ) ?>
                                                    </a>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endwhile ?>
                    </ul>
                </div>

                <div class="search-slider__tabs slider slider--tab">
                    <?php $sliders = get_field( 'sliders' ) ?>
                    <ul class="slider__list js-slider-tab" data-target="#home-slider" id="home-tab-slider" data-number="<?php echo count( $sliders ) ?>">
                        <?php while ( have_rows( 'sliders' ) ) : the_row() ?>
                            <li class="slider__slide slider__slide--tab">
                                <div class="slider__tab">
                                    <div class="slider__tab-title"><?php the_sub_field( 'title' ) ?></div>
                                    <div class="slider__tab-description"><?php the_sub_field( 'description' ) ?></div>
                                </div>
                            </li>
                        <?php endwhile ?>
                    </ul>
                </div>
                <a href="#home-content" class="search-slider__down js-intern-link">
                    <span class="icon icon--chevron-down"></span>
                </a>
            </div>
        <?php endif ?>

        <div class="l-section l-section--stack l-section--home">
            <div class="l-container">
                <h2 class="l-section__title h-color nosproduits__title"><?php the_field( 'card-title_gamme' ) ?></h2>

                <?php if ( have_rows( 'cards_gamme' ) ) : ?>

                    <div class="cards">
                        <div class="l-grid">

                        <?php while ( have_rows( 'cards_gamme' ) ) : the_row() ?>

                            <div class="l-column l-column--tablet-1-3">
                              <a href="<?php the_sub_field('link'); ?>" <?php if (get_sub_field('target')): echo 'target="_blank"'; endif; ?>>
                              <a href="<?php the_sub_field('link'); ?>" title="<?php the_sub_field('title'); ?>">
                                <div class="card card--expanding"
                                    <?php if ( get_sub_field( 'image' ) ): ?>
                                        style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field( 'image' ), 'card' ) ?>)"
                                    <?php endif ?>>
                                    <div class="card__content">
                                        <h3 class="card__title"><?php the_sub_field( 'title' ) ?></h3>
                                        <p class="card__subtitle"><?php the_sub_field( 'subtitle' ) ?></p>
                                        <div class="card__button">
                                          <span class="button button--fullwidth <?php if (get_sub_field('cards_gamme_class')): echo get_sub_field('cards_gamme_class'); endif; ?>">
                                            <?php the_sub_field( 'button' ) ?>
                                          </span>
                                        </div>
                                    </div>
                                    <div class="card__icone">
                                        <?php if ( get_sub_field( 'icone' ) ): ?>
                                            <img src="<?php echo wp_get_attachment_image_url(get_sub_field('icone'), 'card'); ?>" srcset="<?php echo wp_get_attachment_image_url(get_sub_field('icone'), 'card'); ?>" class="icn_sol" />
                                        <?php endif ?>
                                    </div>
                                </div>
                              </a>
                            </div>

                        <?php endwhile ?>

                        </div>
                    </div>

                <?php endif  ?>

            </div>
        </div>

        <div class="l-section l-section--stack" id="home-content">
            <div class="l-container">
                <div class="search-slider__search">
                    <?php get_template_part( 'template-parts/search' ) ?>
                </div>

                <?php if ( get_field( 'home-product-link' ) && get_field( 'home-product-button' ) ) : ?>
                    <div class="search-slider__button search--btn">
                        <div class="text text--center">
                            <a href="<?php the_field( 'home-product-link' ) ?>" class="button">
                                <?php the_field( 'home-product-button' ) ?>
                            </a>
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>

        <?php if(get_the_content()): ?>

            <div class="l-section l-section--stack">
                <div class="l-container">
                    <div class="text">
                        <?php the_content() ?>
                    </div>
                </div>
            </div>

        <?php endif ?>

        <div class="l-section l-section--stack l-section--home">
            <div class="l-container">
                <h2 class="l-section__title h-color"><?php the_field( 'card-title' ) ?></h2>

                <?php if ( have_rows( 'cards' ) ) : ?>

                    <div class="cards">
                        <div class="l-grid">

                        <?php while ( have_rows( 'cards' ) ) : the_row() ?>

                            <div class="l-column l-column--tablet-1-3">
                                <div class="card card--expanding"
                                    <?php if ( get_sub_field( 'image' ) ): ?>
                                        style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field( 'image' ), 'card' ) ?>)"
                                    <?php endif ?>>
                                    <div class="card__content">
                                        <h3 class="card__title"><?php the_sub_field( 'title' ) ?></h3>
                                        <p class="card__subtitle"><?php the_sub_field( 'subtitle' ) ?></p>
                                        <div class="card__button">
                                            <a href="<?php the_sub_field( 'link' ) ?>" class="button button--fullwidth" <?php if ( get_sub_field( 'target' ) ) echo 'target="_blank"' ?>>
                                                <?php the_sub_field( 'button' ) ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card__category">
                                        <?php the_sub_field( 'category' ) ?>
                                    </div>
                                </div>
                            </div>

                        <?php endwhile ?>

                        </div>
                    </div>

                <?php endif  ?>

            </div>
        </div>

        <div class="l-section l-section--space l-section--home">
            <div class="l-container">
                <h2 class="l-section__title h-color"><?php the_field( 'number-title' ) ?></h2>

                <?php if ( have_rows( 'numbers' ) ) : ?>

                    <div class="numbers">

                        <?php while ( have_rows( 'numbers' ) ) : the_row() ?>

                            <div class="number">
                                <div class="number__icon">
                                    <span class="icon icon--<?php the_sub_field( 'icon' ) ?>"></span>
                                </div>
                                <span class="number__suptitle">
                                    <?php the_sub_field( 'suptitle' ) ?>
                                </span>
                                <span class="number__number">
                                    <?php the_sub_field( 'number' ) ?>
                                </span>
                                <span class="number__title">
                                    <?php the_sub_field( 'title' ) ?>
                                </span>
                                <span class="number__subtitle">
                                    <?php the_sub_field( 'subtitle' ) ?>
                                </span>
                            </div>

                        <?php endwhile ?>

                    </div>

                <?php endif ?>

                <div class="l-section__button">
                    <a href="<?php the_field( 'link-bottom' ) ?>" class="button">
                        <?php the_field( 'button-bottom' ) ?>
                    </a>
                </div>

            </div>
        </div>

        <?php while ( have_rows( 'sections' ) ) : the_row() ?>

            <div class="poster"
                <?php if ( get_sub_field( 'background' ) ): ?>
                    style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field( 'background' ), 'poster' ) ?>)"
                <?php endif ?>>
                <div class="l-container">
                    <div class="poster__wrapper">
                        <h2 class="poster__title"><?php the_sub_field( 'title' ) ?></h2>
                        <div class="poster__content">
                            <?php the_sub_field( 'content' ) ?>
                        </div>
                        <div class="poster__button">
                            <a href="<?php the_sub_field( 'link' ) ?>" class="button">
                                <?php the_sub_field( 'button' ) ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        <?php endwhile ?>

    </div>
</div>
