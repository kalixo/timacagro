<?php
$height = get_sub_field( 'height' );
$slick = array();

if ( get_sub_field( 'no-autoplay' ) ) {
	$slick['autoplay'] = false;
}

if ( get_sub_field( 'duration' ) ) {
	$slick['autoplaySpeed'] = get_sub_field( 'duration' );
}
?>
<div class="slider">
	<ul class="slider__list js-slider" <?php if ( $slick ) echo "data-slick='" . esc_attr( json_encode( $slick ) ) . "'" ?>>

		<?php while ( have_rows( 'slider' ) ) : the_row() ?>

			<?php
				// Le slide ouvre une popin
				$is_popin = 'popin' == get_sub_field( 'interaction' );

				// le slide ouvre un lien
				$is_link = 'link' == get_sub_field( 'interaction' ) || $is_popin;

				// le slide a un bouton
				$has_button = 'button' == get_sub_field( 'interaction' );

				// La slide a du contenu a afficher
				$has_content = get_sub_field( 'title' ) || get_sub_field( 'content' ) || $has_button;

				$target = ( get_sub_field( 'target' ) ) ? 'target="_blank"' : '';

				$styles = array();
				// Image d'arrière plan
				if ( get_sub_field( 'image' ) ) {
					$styles[] = 'background-image: url(' . wp_get_attachment_image_url( get_sub_field( 'image' ), 'slider' ) . ')';
				}

				// Hauteur de slides
				if ( $height ) {
					$styles[] = 'height:' . $height . 'px';
				}

			?>
			<li class="slider__slide <?php if ( $height ) echo 'slider__slide--fixed' ?> <?php if ( get_sub_field( 'image-position' ) ) echo 'slider__slide--bg-' . get_sub_field( 'image-position' ) ?>" <?php if ( $styles ) echo 'style="' . implode( ';', $styles ) . '"' ?>>

				<?php if ( $is_link ) : ?>
					<a class="slider__inner slider__inner--link <?php if ( $is_popin ) echo 'js-popin mfp-iframe' ?>" href="<?php the_sub_field( 'link' ) ?>" <?php echo $target ?>>
				<?php else : ?>
					<div class="slider__inner">
				<?php endif ?>

					<?php if ( $has_content ) : ?>

						<div class="slider__content">
							<div class="l-container l-container--small">

								<div class="h-media">
									<div class="h-media__body">
										<div class="slider__title"><?php the_sub_field( 'title' ) ?></div>
										<?php the_sub_field( 'content' ) ?>
									</div>

									<?php if ( $has_button ) : ?>
										<div class="h-media__object h-media__object--middle h-media__object--right">
											<a href="<?php the_sub_field( 'link' ) ?>" class="button" <?php echo $target ?>>
												<?php the_sub_field( 'button' ) ?>
											</a>
										</div>
									<?php endif ?>
								</div>

							</div>
						</div>

					<?php endif ?>

				<?php if ( $is_link ) : ?>
					</a>
				<?php else : ?>
					</div>
				<?php endif ?>


			</li>

		<?php endwhile ?>

	</ul>
</div>
