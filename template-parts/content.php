<div <?php post_class() ?>>

	<?php get_template_part( 'template-parts/header' ) ?>

	<div class="page__content">

		<?php if ( get_the_content() ) : ?>

			<div class="l-section l-section--stack">
				<div class="l-container">
					<div class="text">
						<?php the_content() ?>
					</div>
				</div>
			</div>

		<?php endif ?>

		<?php get_template_part( 'template-parts/layout' ) ?>

	</div>
</div>