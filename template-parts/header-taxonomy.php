<?php
global $wp_query;

// Identifiant ACF
$tax = $wp_query->queried_object;

// Couleur du produit
$color = 'gray';
if ( get_field( 'header-color', $tax ) ) {
    $color = get_field( 'header-color', $tax );
}
$color_class = 'h-background--' . $color;

// Lien de retour
$back_url = null;
if ( $tax->parent ) {
    $back_url = timacagro_filter_link( get_term_link( $tax->parent ) );
}


if ( ! get_field( 'header-hide', $tax ) ) : ?>
    <div class="page__header header h-background <?php echo $color_class ?>">
        <div class="l-container">
            <div class="header__top">

                <?php if ( $back_url ) : ?>
                    <a href="<?php echo esc_url( $back_url ) ?>" class="button button--outline button--small button--with-icon header__back">
                        <span class="icon icon--chevron-left"></span>
                        <?php _e( 'Retour', 'timacagro' ) ?>
                    </a>
                <?php endif ?>


                <?php if ( function_exists( 'bcn_display' ) ) : ?>
                    <!-- Fil d'ariane -->
                    <div class="header__breadcrumb breadcrumb" typeof="BreadcrumbList" vocab="http://schema.org/">
                        <?php bcn_display() ?>
                    </div>
                <?php endif ?>

            </div>

            <div class="header__content header__content__breadcrumb">

                <?php if ( get_field( 'header-icon', $tax ) ) : ?>
                    <?php echo wp_get_attachment_image( get_field( 'header-icon', $tax ), 'full', false, array( 'class' => 'header__icon' ) ) ?>
                <?php endif ?>

                <?php $show_title_breadcrumb 	= true; ?>
                <?php $show_as_subtitle 		 	= false; ?>

                <?php if(isset($tax->parent) && $tax->parent > 0): ?>
                    <?php $parent_term_breadcrumb = get_term_by('id', $tax->parent, 'product_range'); ?>
                    <?php if(isset($parent_term_breadcrumb->parent) && $parent_term_breadcrumb->parent > 0): ?>
                        <?php $parent_term_breadcrumb_top = get_term_by('id', $parent_term_breadcrumb->parent, 'product_range'); ?>
                        <span class="header__title page__title parent__breadcrumb">
                            <a href="<?php echo get_term_link($parent_term_breadcrumb_top); ?>" title="<?php echo $parent_term_breadcrumb_top->name; ?>">
                                <?php echo $parent_term_breadcrumb_top->name; ?>
                            </a>
                        </span>
                        <?php $show_title_breadcrumb = false; ?>
                        <?php $show_as_subtitle = true; ?>
                    <?php endif ?>
                    <?php if($show_as_subtitle == false): ?>
                        <span class="header__title page__title parent__breadcrumb">
                            <a href="<?php echo get_term_link($parent_term_breadcrumb); ?>" title="<?php echo $parent_term_breadcrumb->name; ?>">
                                <?php echo $parent_term_breadcrumb->name; ?>
                            </a>
                        </span>
                    <?php else: ?>
                        <span class="header__subtitle"><span>&nbsp;>&nbsp;</span>
                            <a href="<?php echo get_term_link($parent_term_breadcrumb); ?>" title="<?php echo $parent_term_breadcrumb->name; ?>">
                                <?php echo $parent_term_breadcrumb->name; ?>
                            </a>
                        </span>
                    <?php endif ?>
                    <?php $show_as_subtitle = true; ?>
                <?php endif ?>

                <!-- Titre -->
                <?php if($show_title_breadcrumb == true): ?>
                    <!-- <h2 class="header__title page__title parent__breadcrumb"><span>&nbsp;>&nbsp;</span></h2> -->
                    <?php if($show_as_subtitle == false): ?>
                        <span class="header__title page__title"><?php single_term_title() ?></span>
                    <?php else: ?>
                        <span class="header__subtitle">&nbsp;></span><h1 class="header__subtitle"><?php single_term_title() ?></h1>
                    <?php endif; ?>
                <?php endif; ?>
                <!-- Sous titre -->
                <?php if ( get_field( 'header-subtitle', $tax ) ) : ?>
                    <h1 class="header__subtitle"><?php the_field( 'header-subtitle', $tax ) ?></h1>
                <?php endif ?>

            </div>
        </div>
    </div>

<?php endif ?>
