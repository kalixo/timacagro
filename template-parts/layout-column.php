    <?php
        if (get_field('type_de_layout') && get_field('type_de_layout') == 'mosaic'):
            echo '<div class="l-container">';
            echo '<h2 class="l-section__title h-color nosproduits__title">'.__('Notre métier, optimiser la synergie ', 'timac-agro').'<strong>'.__('Terre/Végétal/Animal', 'timac-agro').'</strong></h2>';
        else:
            echo '<div class="l-column l-column--desktop-'.get_sub_field( 'width' ).'">';
        endif;
    ?>

    <?php if ( have_rows( 'content' ) ) : ?>

        <div class="cards col_cards cards-center">
            <div class="l-grid">
                <?php while ( have_rows( 'content' ) ) : the_row() ?>
                    <?php
                        if (get_field('type_de_layout') && get_field('type_de_layout') == 'mosaic'):
                            echo '<div class="l-column l-column--tablet-1-3">';
                        endif;
                    ?>
                        <?php get_template_part( 'template-parts/layout', get_row_layout() ) ?>
                    <?php
                        if (get_field('type_de_layout') && get_field('type_de_layout') == 'mosaic'):
                            echo '</div>';
                        endif;
                    ?>
                <?php endwhile ?>
            </div>
        </div>

    <?php endif ?>

</div>
