<?php if ( have_rows( 'number' ) ) : ?>

	<div class="numbers">

		<?php while ( have_rows( 'number' ) ) : the_row() ?>

			<div class="number">
				<?php if ( get_sub_field( 'icon' ) ) : ?>
					<div class="number__icon">
						<?php if ( 'image' == get_sub_field( 'icon' ) && $image = get_sub_field( 'image' ) ) : ?>
							<?php echo wp_get_attachment_image( $image, 'full', false, array( 'class' => 'number__image' ) ) ?>
						<?php else : ?>
							<span class="icon icon--<?php the_sub_field( 'icon' ) ?>"></span>
						<?php endif ?>
					</div>
				<?php endif ?>
				<span class="number__suptitle">
					<?php the_sub_field( 'suptitle' ) ?>
				</span>
				<span class="number__number">
					<?php the_sub_field( 'number' ) ?>
				</span>
				<span class="number__title">
					<?php the_sub_field( 'title' ) ?>
				</span>
				<span class="number__subtitle">
					<?php the_sub_field( 'subtitle' ) ?>
				</span>
			</div>

		<?php endwhile ?>

	</div>

<?php endif ?>
