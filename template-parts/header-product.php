<?php
// La gamme du produit
$product_range = null;
$terms = get_the_terms( get_the_ID(), 'product_range' );
if ( ! is_wp_error( $terms ) && $terms ) {
    $product_range = $terms[0];
}

// Lien de retour
$back_url = get_post_type_archive_link( 'product' );
if ( $product_range ) {
    $back_url = get_term_link( $product_range );
}
$back_url = timacagro_filter_link( $back_url );

// Couleur du produit
$color = 'gray';
if ( get_field( 'header-color', $product_range ) ) {
    $color = get_field( 'header-color', $product_range );
}
if ( get_field( 'header-color' ) ) {
    $color = get_field( 'header-color' );
}
$color_class = 'h-background--' . $color;

$product_demo_button = get_field( 'product-demo-button' ) ? get_field( 'product-demo-button' ) : get_field( 'product-demo-button', 'options' );
$product_testimonial_button = get_field( 'product-testimonial-button' ) ? get_field( 'product-testimonial-button' ) : get_field( 'product-testimonial-button', 'options' );

if ( ! get_field( 'header-hide' ) ) : ?>

    <div class="page__header header h-background <?php echo $color_class ?>">
        <div class="l-container">
            <div class="header__container">
                <div class="header__top">
                    <a href="<?php echo esc_url( $back_url ) ?>" class="button button--outline button--small button--with-icon header__back">
                        <span class="icon icon--chevron-left"></span>
                        <?php _e( 'Retour', 'timacagro' ) ?>
                    </a>

                    <?php if ( function_exists( 'bcn_display' ) ) : ?>
                        <!-- Fil d'ariane -->
                        <div class="header__breadcrumb breadcrumb" typeof="BreadcrumbList" vocab="http://schema.org/">
                            <?php bcn_display() ?>
                        </div>
                    <?php endif ?>
                </div>

                <div class="header__content header__content__breadcrumb">
                    <?php if ( get_field( 'header-icon', $product_range ) ) : ?>
                        <?php echo wp_get_attachment_image( get_field( 'header-icon', $product_range ), 'full', false, array( 'class' => 'header__icon' ) ) ?>
                    <?php endif ?>

                    <?php $show_title_breadcrumb = true; ?>

                    <?php if(isset($product_range->parent) && $product_range->parent > 0): ?>
                        <?php $parent_term_breadcrumb = get_term_by('id', $product_range->parent, 'product_range'); ?>
                        <?php if(isset($parent_term_breadcrumb->parent) && $parent_term_breadcrumb->parent > 0): ?>
                            <?php $parent_term_breadcrumb_top = get_term_by('id', $parent_term_breadcrumb->parent, 'product_range'); ?>
                            <span class="header__title page__title parent__breadcrumb">
                                <a href="<?php echo get_term_link($parent_term_breadcrumb_top); ?>" title="<?php echo $parent_term_breadcrumb_top->name; ?>">
                                    <?php echo $parent_term_breadcrumb_top->name; ?>
                                </a>
                            </span>
                            <?php $show_title_breadcrumb = false; ?>
                        <?php endif ?>
                        <span class="header__subtitle">
                            <a href="<?php echo get_term_link($parent_term_breadcrumb); ?>" title="<?php echo $parent_term_breadcrumb->name; ?>">
                                <span>&nbsp;>&nbsp;</span> <?php echo $parent_term_breadcrumb->name; ?>
                            </a>
                        </span>
                    <?php endif ?>
                    <span class="header__subtitle"><span>&nbsp;>&nbsp;</span>
                      <a href="<?php echo get_term_link($product_range); ?>" title="<?php echo $product_range->name; ?>">
                        <?php echo $product_range->name; ?>
                      </a>
                   </span>
                </div>

                <?php if ( get_field( 'product-demo' ) || get_field( 'product-testimonial' ) ) : ?>

                    <div class="header__right">

                        <?php if ( get_field( 'product-demo' ) ) : ?>
                            <a href="<?php the_field( 'product-demo-link' ) ?>" class="button js-popin mfp-iframe product__demo">
                                <?php echo $product_demo_button ?>
                                <span class="icon icon--video"></span>
                            </a>
                        <?php endif ?>

                        <?php if ( get_field( 'product-testimonial' ) ) : ?>
                            <a href="<?php the_field( 'product-testimonial-link' ) ?>" class="button js-popin mfp-iframe">
                                <?php echo $product_testimonial_button ?>
                                <span class="icon icon--video"></span>
                            </a>
                        <?php endif ?>

                    </div>

                <?php endif ?>
            </div>
        </div>
    </div>

<?php endif ?>
