<div class="card"
    <?php if ( has_post_thumbnail() ): ?>
        style="background-image: url(<?php the_post_thumbnail_url( 'card' ) ?>)"
    <?php endif ?>>
    <div class="card__content">
        <h3 class="card__title"><?php the_title() ?></h3>
        <div class="card__subtitle"><?php the_excerpt() ?></div>
        <div class="card__button">
            <a href="<?php the_permalink() ?>" class="button button--fullwidth">
                <?php _e( 'En savoir plus', 'timacagro' ) ?>
            </a>
        </div>
    </div>
</div>
