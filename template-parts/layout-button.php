<?php

// Pour le conteneur
$classes = array( 'text' );
if ( get_sub_field( 'align' ) ) {
	$classes[] = 'text--' . get_sub_field( 'align' );
}

// Pour le lien
$target = '';
if ( get_sub_field( 'target' ) ) {
	$target = 'target="_blank"';
}

$link = get_sub_field( 'link' );
$classes_link = array( 'button' );
if ( 'intern' == get_sub_field( 'type' ) ) {
	$link = get_sub_field( 'link-intern' );
}

if ( 'video' == get_sub_field( 'type' ) ) {
	$classes_link[] = 'js-popin';
	$classes_link[] = 'mfp-iframe';
}

if ( 'ajax' == get_sub_field( 'type' ) ) {
	$classes_link[] = 'js-popin';
	$classes_link[] = 'mfp-ajax';
	$link = timacagro_context_url( get_sub_field( 'link-intern' ) );
}

$tracking = '';
if ( get_sub_field( 'tracking' ) ) {
	$tracking = 'onclick="' . get_sub_field( 'tracking' ) . '"';
}

?>

<div class="<?php echo implode( ' ', $classes ) ?>">
	<a href="<?php echo esc_url( $link ) ?>" class="<?php echo implode( ' ', $classes_link ) ?>" <?php echo $target ?> <?php echo $tracking ?>>
		<?php the_sub_field( 'button' ) ?>
	</a>
</div>