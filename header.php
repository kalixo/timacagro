<?php
/**
 * The template for displaying the header
 *
 * @package WordPress
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-33854273-1', 'auto');
        ga('send', 'pageview');
    </script>
</head>

<body <?php body_class(); ?>>
<div id="cookies-toolbar">
    <span>Le site timacagro.fr utilise des cookies afin d'améliorer votre expérience de navigation. En poursuivant votre navigation sur ce site, vous en acceptez l'utilisation.</span>
    <a href="#" class="close-ctoolbar ctoolbar-link" title="Fermer ce bandeau">
        <span>Fermer</span>
    </a>
</div>
<div id="page" class="site">
    <header class="site__header">
        <div class="l-container">
            <div class="logo_mobile">
                <a href="<?php bloginfo('url') ?>" class="site__home"><?php bloginfo('name') ?></a>
            </div>
            <nav class="site__navigation" role="navigation" aria-label="<?php esc_attr_e( 'Menu primaire', 'timac-agro' ); ?>">
                <?php wp_nav_menu( array('theme_location' => 'primary' ) ); ?>
            </nav><!-- .navigation -->

            <?php
                if(get_field('afficher_telephone', 'option') == 'oui'):
                    $hidden = false;
                    if(get_field('pages_a_exclures', 'option')):
                        foreach(get_field('pages_a_exclures', 'option') as $page):
                    if(isset($page['page']) && !empty($page['page']) && $page['page'] == get_the_ID()):
                                $hidden = true;
                            endif;
                    endforeach;
                    endif;
                    if($hidden == false):
            ?>
                        <div class="site__phone">
                            <div class="prefix"><?php echo the_field('prefix', 'option'); ?></div>
                            <div class="phone"><strong><?php echo the_field('telephone', 'option'); ?></strong></div>
                            <div class="sufix"><?php echo the_field('sufix', 'option'); ?></div>
                        </div>
            <?php
                    endif;
                endif;
                $hidden = false;
                foreach(get_field('pages_a_exclures_liens_lateraux', 'option') as $page):
                    if(isset($page['page']) && !empty($page['page']) && $page['page'] == get_the_ID()):
                        $hidden = true;
                    endif;
                endforeach;
                if($hidden == false):
              if(have_rows('asides', 'options')) : ?>
                        <div class="site__aside site__aside__desktop">
                            <div class="aside">
                                <?php while ( have_rows( 'asides', 'options' ) ) : the_row() ?>
                                    <?php
                                    $link = get_sub_field( 'link' );
                                    if ( get_sub_field( 'popin' ) ) {
                                        $link = timacagro_context_url( $link );
                                    }
                                    ?>
                                    <a href="<?php echo esc_url( $link ) ?>" class="aside__item <?php if ( get_sub_field( 'popin' ) ) echo 'js-popin mfp-ajax' ?> kxowave<?php echo $ii++ ; ?>" <?php if ( get_sub_field( 'popin' ) ) echo "onclick=\"ga('send','event','CTA-RDV-expert','Clic','CTA',1);\"" ?> >
                                            <?php if ( get_sub_field( 'icon' ) ) : ?>
                                                <span class="aside__icon">
                                                    <span class="icon icon--<?php the_sub_field( 'icon' ) ?>"></span>
                                                </span>
                                            <?php endif ?>
                                        <span class="aside__text">
                                            <?php the_sub_field( 'text' ) ?>
                                        </span>
                                    </a>
                                <?php endwhile ?>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            <?php if(have_rows('asides','options')): ?>
                <div class="site__aside site__aside__mobile">
                    <div class="aside">
                        <?php while ( have_rows( 'asides', 'options' ) ) : the_row() ?>
                            <?php
                            $link = get_sub_field( 'link' );
                            if ( get_sub_field( 'popin' ) ) {
                                $link = timacagro_context_url( $link );
                            }
                            ?>
                            <a href="<?php echo esc_url( $link ) ?>" class="aside__item <?php if ( get_sub_field( 'popin' ) ) echo 'js-popin mfp-ajax' ?> kxowave<?php echo $ii++ ; ?>" <?php if ( get_sub_field( 'popin' ) ) echo "onclick=\"ga('send','event','CTA-RDV-expert','Clic','CTA',1);\"" ?> >
                                            <?php if ( get_sub_field( 'icon' ) ) : ?>
                                                <span class="aside__icon">
                                                    <span class="icon icon--<?php the_sub_field( 'icon' ) ?>"></span>
                                                </span>
                                            <?php endif ?>
                                <span class="aside__text">
                                    <?php the_sub_field( 'text' ) ?>
                                </span>
                            </a>
                        <?php endwhile ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>

    </header>
    <div class="site__content">
