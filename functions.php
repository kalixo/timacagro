<?php

/**
 * Largeur du contenu
 */
if ( ! isset( $content_width ) )
	$content_width = 1080;

/**
 * Fonctionnalités du thème.
 *
 * @return void
 */
function timacagro_setup()  {

	// Add theme support for Automatic Feed Links
	add_theme_support( 'automatic-feed-links' );

	// Add theme support for Featured Images
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'slider', 1600, 740, true );
	add_image_size( 'poster', 1600, 520, true );
	add_image_size( 'card', 459, 612, true );

	// Add theme support for HTML5 Semantic Markup
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

	// Add theme support for document Title tag
	add_theme_support( 'title-tag' );

	// Add theme support for Translation
	load_theme_textdomain( 'timac-agro', get_template_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'timacagro_setup' );

/**
 * Qualité des images
 *
 * @since 1.0
 */
function timacagro_image_quality() {
    return 100;
}
add_filter( 'jpeg_quality', 'timacagro_image_quality');

/**
 * Définition des emplacements de menu.
 *
 * @return void
 */
function timacagro_menus() {

	$locations = array(
		'primary' => __( 'Menu principal', 'timac-agro' ),
		'footer'  => __( 'Menu de pied de page', 'timac-agro' ),
	);
	register_nav_menus( $locations );

}
add_action( 'init', 'timacagro_menus' );

/**
 * Ajout des styles et scripts.
 *
 * @return  void
 */
function timacagro_scripts() {
	// Police de caractère Google Fonts.
	wp_enqueue_style( 'google-fonts', "https://fonts.googleapis.com/css?family=Khand:400,600,700", array(), null );

	// Style du thème
	wp_enqueue_style( 'timacagro-style', get_stylesheet_uri(), array(), '1.9' );

	// Polyfill
	wp_enqueue_script( 'html5shiv', 'https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js' );
	wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
	wp_enqueue_script( 'respond', 'https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js' );
	wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

	// Script du thème
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/js/slick.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array( 'jquery' ), null, true );
	wp_enqueue_script( 'masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array( 'jquery', 'imagesloaded' ), null, true );
	wp_enqueue_script( 'timacagro-script', get_template_directory_uri() . '/js/main.js', array( 'jquery', 'slick', 'magnific-popup', 'masonry' ), '1.3', true );
}
add_action( 'wp_enqueue_scripts', 'timacagro_scripts' );

/**
 * Ajout d'un script manuel après les CSS.
 *
 * @return void
 */
function timacagro_nojs_script()
{
	echo "<script>(function(h){h.className = h.className.replace('no-js', 'js')})(document.documentElement)</script>";

}
add_action( 'wp_head', 'timacagro_nojs_script' );

/**
 * Désactivation des emoticons.
 *
 * @return void
 */
function timacagro_disable_wp_emojicons() {

	// all actions related to emojis
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

	// filter to remove TinyMCE emojis
	add_filter( 'tiny_mce_plugins', 'timacagro_disable_emojicons_tinymce' );
}
add_action( 'init', 'timacagro_disable_wp_emojicons' );
add_filter( 'emoji_svg_url', '__return_false' );

/**
 * Désactivation des emoticons.
 *
 * @return void
 */
function timacagro_disable_emojicons_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Pages d'options ACF pour le thème
 */
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page( array(
		'page_title' => 'Global',
		'capability' => 'manage_options'
	) );
}

/**
 * Sauvegarde des champs ACF en JSON.
 *
 * @param  string $path
 * @return string
 */
function timacagro_acf_json_save_point( $path ) {

    // update path
    $path = get_stylesheet_directory() . '/acf-fields';
    return $path;
}
add_filter('acf/settings/save_json', 'timacagro_acf_json_save_point');

/**
 * Chargement des champs ACF en JSON.
 *
 * @param  string $path
 * @return string
 */
function timacagro_acf_json_load_point( $paths ) {

    // remove original path (optional)
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/acf-fields';
    return $paths;
}
add_filter('acf/settings/load_json', 'timacagro_acf_json_load_point');

/**
 * Produits.
 *
 * @return void
 */
function timacagro_post_type_product() {

	$labels = array(
		'name'                  => _x( 'Produits', 'Post Type General Name', 'timacagro' ),
		'singular_name'         => _x( 'Produit', 'Post Type Singular Name', 'timacagro' ),
		'menu_name'             => __( 'Produits', 'timacagro' ),
		'name_admin_bar'        => __( 'Produit', 'timacagro' ),
	);
	$rewrite = array(
		'slug'                  => 'produit',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$args = array(
		'label'                 => __( 'Produit', 'timacagro' ),
		'description'           => __( 'Produit TIMAC AGRO', 'timacagro' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'author', 'thumbnail', 'revisions', 'page-attributes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-cart',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'product',
		'map_meta_cap'          => true
	);
	register_post_type( 'product', $args );
}
add_action( 'init', 'timacagro_post_type_product', 0 );

function timacagro_role_admin_capabilities()
{
	global $wp_post_types;
	$cap = $wp_post_types['product']->cap;

	$admin = get_role( 'administrator' );
	if ( $admin ) {
		$admin->add_cap( 'read_private_products' );
		$admin->add_cap( 'edit_products' );
		$admin->add_cap( 'edit_others_products' );
		$admin->add_cap( 'edit_private_products' );
		$admin->add_cap( 'edit_published_products' );
		$admin->add_cap( 'publish_products' );
		$admin->add_cap( 'delete_products' );
		$admin->add_cap( 'delete_private_products' );
		$admin->add_cap( 'delete_published_products' );
		$admin->add_cap( 'delete_others_products' );
	}
}
add_action( 'admin_init', 'timacagro_role_admin_capabilities' );

/**
 * Ajout du rôle "Marketing produits".
 *
 * @return void
 */
function timacagro_role_product_marketing()
{
	if ( get_role( 'product_marketing' ) )
		return;

	$result = add_role(
	    'product_marketing',
	    __( 'Marketing Produits', 'timacagro' ),
	    array(
	        'read'            => true,
	        'edit_products'   => true,
	        'delete_products' => true,
	        'upload_files'    => true,
	        'assign_terms'    => true,
	    )
	);
}
add_action( 'admin_init', 'timacagro_role_product_marketing' );


/**
 * Gamme de produits.
 *
 * @return void
 */
function timacagro_taxonomy_product_range() {

	$labels = array(
		'name'                       => _x( 'Gammes', 'Taxonomy General Name', 'timacagro' ),
		'singular_name'              => _x( 'Gamme', 'Taxonomy Singular Name', 'timacagro' ),
		'menu_name'                  => __( 'Gammes', 'timacagro' ),
		'all_items'                  => __( 'Toutes les gammes', 'timacagro' ),
		'parent_item'                => __( 'Gamme parente', 'timacagro' ),
		'parent_item_colon'          => __( 'Gamme parente :', 'timacagro' ),
		'new_item_name'              => __( 'Nom de la gamme', 'timacagro' ),
		'add_new_item'               => __( 'Ajouter une gamme', 'timacagro' ),
		'edit_item'                  => __( 'Modifier la gamme', 'timacagro' ),
		'update_item'                => __( 'Mettre à jour la gamme', 'timacagro' ),
		'view_item'                  => __( 'Voir la gamme', 'timacagro' ),
	);
	$rewrite = array(
		'slug'                       => 'gamme',
		'with_front'                 => true,
		'hierarchical'               => true,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => false,
		'rewrite'                    => $rewrite,
		'capabilities' => array(
			'manage_terms' => 'publish_products',
			'edit_terms'   => 'publish_products',
			'delete_terms' => 'publish_products',
			'assign_terms' => 'edit_products'
		),
	);
	register_taxonomy( 'product_range', array( 'product' ), $args );

}
add_action( 'init', 'timacagro_taxonomy_product_range', 0 );

/**
 * Besoin sur les produits.
 *
 * @return void
 */
function timacagro_taxonomy_product_need() {

	$labels = array(
		'name'                       => _x( 'Besoins', 'Taxonomy General Name', 'timacagro' ),
		'singular_name'              => _x( 'Besoin', 'Taxonomy Singular Name', 'timacagro' ),
		'menu_name'                  => __( 'Besoins', 'timacagro' ),
		'all_items'                  => __( 'Tous les besoins', 'timacagro' ),
		'add_new_item'               => __( 'Ajouter un besoin', 'timacagro' ),
	);
	$rewrite = array(
		'slug'                       => 'besoin',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'query_var'                  => 'product_need',
		'rewrite'                    => $rewrite,
		'capabilities' => array(
			'manage_terms' => 'publish_products',
			'edit_terms'   => 'publish_products',
			'delete_terms' => 'publish_products',
			'assign_terms' => 'edit_products'
		),
	);
	register_taxonomy( 'product_need', array( 'product' ), $args );

}
add_action( 'init', 'timacagro_taxonomy_product_need', 0 );

/**
 * Profile sur les produits.
 *
 * @return void
 */
function timacagro_taxonomy_product_profile() {

	$labels = array(
		'name'                       => _x( 'Profils', 'Taxonomy General Name', 'timacagro' ),
		'singular_name'              => _x( 'Profil', 'Taxonomy Singular Name', 'timacagro' ),
		'menu_name'                  => __( 'Profils', 'timacagro' ),
		'all_items'                  => __( 'Tous les profils', 'timacagro' ),
		'add_new_item'               => __( 'Ajouter un profil', 'timacagro' ),
	);
	$rewrite = array(
		'slug'                       => 'profil',
		'with_front'                 => true,
		'hierarchical'               => false,
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => false,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => false,
		'show_tagcloud'              => false,
		'query_var'                  => 'product_profile',
		'rewrite'                    => $rewrite,
		'capabilities' => array(
			'manage_terms' => 'publish_products',
			'edit_terms'   => 'publish_products',
			'delete_terms' => 'publish_products',
			'assign_terms' => 'edit_products'
		),
	);
	register_taxonomy( 'product_profile', array( 'product' ), $args );

}
add_action( 'init', 'timacagro_taxonomy_product_profile', 0 );

/**
 * Relation entre les profiles et les termes.
 *
 * @param  multiple $value
 * @param  int      $acf_term_id
 * @param  array    $field
 * @return multiple
 */
function timacagro_related_taxonomies( $value, $acf_term_id, $field ) {

	$taxonomy = explode( '_', $acf_term_id );
	$term_id = array_pop( $taxonomy );
	$taxonomy = implode( '_', $taxonomy );
	$taxonomy_2 = $field['taxonomy'];

	$field_name = $field['name'];

	$global_name = 'is_updating_' . $field_name;

	// Pas de boucle infinie
	if( !empty($GLOBALS[ $global_name ]) ) return $value;
	$GLOBALS[ $global_name ] = 1;

	// On parcoure les valeurs saisies
	if( is_array( $value ) ) {
		foreach( $value as $term_id_2 ) {
			$acf_term_id_2 = sprintf( '%s_%d', $taxonomy_2, $term_id_2 );

			// On charge les valeurs existantes
			$value_2 = get_field( $field_name, $acf_term_id_2, false );

			// Il est possible que la valeur soit vide
			if( empty( $value_2 ) ) {
				$value_2 = array();
			}

			// Si la taxonomie est déjà présente, on ne fait rien
			if( in_array( $term_id, $value_2 ) ) continue;

			// On ajoute le terme courant à la liste
			$value_2[] = $term_id;

			// On met à jour le champ
			update_field( $field_name, $value_2, $acf_term_id_2 );
		}
	}

	// On met à jour les suppressions possibles
	$old_value = get_field($field_name, $acf_term_id, false);

	if( is_array($old_value) ) {
		foreach( $old_value as $term_id_2 ) {
			// On ne fait rien si le contenu n'a pas été supprimé
			if( is_array( $value ) && in_array( $term_id_2, $value ) ) continue;

			$acf_term_id_2 = sprintf( '%s_%d', $taxonomy_2, $term_id_2 );

			// On charge les valeurs existantes
			$value_2 = get_field( $field_name, $acf_term_id_2, false );

			// Pas de données dans la valeur précédente : on continue
			if( empty( $value_2 ) ) continue;

			// On retire la valeur présente
			$pos = array_search( $term_id, $value_2 );
			unset( $value_2[ $pos] );

			// On met à jour le champ
			update_field( $field_name, $value_2, $acf_term_id_2 );
		}
	}

	// Retour à la normal
	$GLOBALS[ $global_name ] = 0;

    return $value;
}
add_filter('acf/update_value/name=related-taxonomies', 'timacagro_related_taxonomies', 10, 3);


/**
 * Vidéo responsive en HTML5.
 *
 * @param  string $html
 * @return string
 */
function timacagro_embed_html( $html ) {
    return '<div class="video-container">' . $html . '</div>';
}

add_filter( 'embed_oembed_html', 'timacagro_embed_html', 10, 3 );
add_filter( 'video_embed_html', 'timacagro_embed_html' ); // Jetpack

/**
 * Mise a jour de la couleur d'entête pour les produits et les gammes.
 *
 * @param  multi  $value
 * @param  string $post_id
 * @param  array  $field
 * @return multi
 */
function timacagro_update_header_color( $value, $post_id, $field )
{
	$field_name = $field['name'];
	$field_key = $field['key'];
	$global_name = 'is_updating_' . $field_name . '_' . $post_id;

	// Evite les boucles infinies
	if( !empty($GLOBALS[ $global_name ]) ) return $value;
	$GLOBALS[ $global_name ] = 1;

	// vars
	$glue = '_';
	$type = explode($glue, $post_id);
	$id = array_pop($type);
	$type = implode($glue, $type);

	// Uniquement pour les gammes
	if ( is_numeric($id) && 'product_range' == $type )
	{
		// Mise à jour des gammes enfants
		$terms = get_terms( array(
			'taxonomy'   => 'product_range',
			'hide_empty' => false,
			'parent'     => $id,
		) );

		foreach ( $terms as $term ) {
			update_field( $field_key, $value, $term );
		}

		// Mise à jour des produits enfants
		$products = get_posts(array(
			'post_type' => 'product',
			'numberposts' => -1,
			'tax_query' => array(
				array(
					'taxonomy' => 'product_range',
					'terms' => $id,
					'include_children' => false
				)
			)
		));
		foreach ( $products as $product ) {
			update_field( $field_key, $value, $product );
		}
	}

	$GLOBALS[ $global_name ] = 0;

	return $value;
}
add_filter('acf/update_value/name=header-color', 'timacagro_update_header_color', 10, 3);

/**
 * Positionnement de la balise "noindex" lorsque l'un des filtres "need" ou "profile" sont
 * présent dans l'URL.
 *
 * @param  string $robotsstr
 * @return string
 */
function timacagro_noindex_on_filter( $robotsstr )
{
	if ( isset( $_GET['product_need'] ) || isset( $_GET['product_profile'] ) || isset( $_GET['product_range'] ) ) {
		$robotsstr = 'noindex';
	}

	return $robotsstr;
}
add_filter( 'wpseo_robots', 'timacagro_noindex_on_filter' );

/**
 * Redirection 301 des sous pages de gammes / produits / besoins.
 *
 * @return void
 */
function timacagro_redirect_paginated_page()
{
	global $wp, $wp_query;

	// On a une pagination
	if ( ! is_admin() && is_paged() && (
			// On est sur la page d'accueil
			( is_front_page() && ! is_home() )
			||
			// On est sur une page sans pagination
			( -1 == $wp_query->get( 'posts_per_page' ) )
		)
	 ) {
	 	// On récupère les variables
	 	$vars = $wp->query_vars;
	 	unset($vars['paged']);
		$current_url = add_query_arg( $vars, home_url() );
		wp_redirect( $current_url, 301 );
		exit;
	}
}
add_action('template_redirect', 'timacagro_redirect_paginated_page' );
/**
 * On limite l'affichage des produits associés à une gamme uniquement au "descendant direct"
 * de la gamme.
 *
 * @param  WP_Query $query
 * @return void
 */
function timacagro_posts_in_product_range( $query )
{
	if ( ! is_admin() && $query->is_main_query() && $query->is_tax( 'product_range' ) ) {
		$query->tax_query->queries[0]['include_children'] = false;
	}
}
// add_action( 'parse_tax_query', 'timacagro_posts_in_product_range' );

/**
 * Changement du nombre de produit par page dans les templates "archive".
 *
 * @param  WP_Query $query
 * @return void
 */
function timacagro_posts_per_pages_product( $query )
{
	if ( ! is_admin() && $query->is_main_query() && is_post_type_archive( 'product' ) ) {
		$query->set( 'posts_per_page', '12' );
		$query->set( 'tax_query', timacagro_product_tax_query() );
	}
}
add_action( 'pre_get_posts', 'timacagro_posts_per_pages_product' );

/**
 * Changement du nombre de produit par page dans les templates "archive".
 *
 * @param  WP_Query $query
 * @return void
 */
function timacagro_posts_per_pages_product_range( $query )
{
	if ( ! is_admin() && $query->is_main_query() && is_tax( 'product_range' ) ) {
		$query->set( 'posts_per_page', '-1' );
	}
}
add_action( 'pre_get_posts', 'timacagro_posts_per_pages_product_range' );

/**
 * Shortcode pour afficher un menu dans une page.
 *
 * @param  array $atts
 * @param  string $content
 * @return string
 */
function timacagro_shortcode_menu( $atts, $content = null )
{
    extract( shortcode_atts( array(
    	'name' => null,
    ), $atts));

    return wp_nav_menu( array( 'menu' => $name, 'echo' => false ) );
}
add_shortcode('menu', 'timacagro_shortcode_menu');

/**
 * Shortcode pour afficher une iframe avec le bon context.
 *
 * @param  array $atts
 * @param  string $content
 * @return string
 */
function timacagro_shortcode_form_iframe( $atts, $content = null )
{
	extract( shortcode_atts( array(
		'height' => 400,
		'url' => 'https://web.timacagro.fr/rdv_expert',
		'form' => 1,
		'from' => timacagro_current_url()
	), $atts));

	// Contexte du formulaire
	$context = isset( $_GET['context-page'] ) ? $_GET['context-page'] : $from;

	// Contextualisation du lien
	$url = add_query_arg( array( 'from' => $context, 'form' => $form ), $url );

	return sprintf( '<iframe style="width: 100%%;" src="%s" width="600" height="%s"></iframe>', $url, $height );
}
add_shortcode( 'form_iframe', 'timacagro_shortcode_form_iframe' );

/**
 * Récupère dans les paramètres GET le filtre "product_need"
 *
 * @return string|boolean
 */
function timacagro_get_product_need_filter()
{
	if ( isset( $_GET['product_need'] ) && $_GET['product_need'] ) {
		return $_GET['product_need'];
	}

	return false;
}

/**
 * Récupère dans les paramètres GET le filtre "product_profile"
 *
 * @return string|boolean
 */
function timacagro_get_product_profile_filter()
{
	if ( isset( $_GET['product_profile'] ) && $_GET['product_profile'] ) {
		return $_GET['product_profile'];
	}

	return false;
}

/**
 * Créer la requete taxonomique pour les produits en fonction du besoin / profile / gamme.
 *
 * @return array
 */
function timacagro_product_tax_query( $include_children = true )
{
	$tax_query = array( 'relation' => 'AND' );
	if ( $product_need = timacagro_get_product_need_filter() ) {
		$tax_query[] = array(
			'taxonomy' => 'product_need',
			'field'    => 'slug',
			'terms'    => $product_need,
		);
	}

	if ( $product_profile = timacagro_get_product_profile_filter() ) {
		$tax_query[] = array(
			'taxonomy' => 'product_profile',
			'field'    => 'slug',
			'terms'    => $product_profile,
		);
	}

	if ( get_query_var( 'product_range' ) ) {
		$tax_query[] = array(
			'include_children' => $include_children,
			'taxonomy'         => 'product_range',
			'field'            => 'slug',
			'terms'            => get_query_var( 'product_range' ),
		);
	}

	return $tax_query;
}

/**
 * Ajoute les paramètres "product_profile" et "product_need" au lien transmis en paramètre.
 *
 * @param  string $link
 * @return string
 */
function timacagro_filter_link( $link )
{
	return add_query_arg(
		array(
			'product_profile' => timacagro_get_product_profile_filter(),
			'product_need' => timacagro_get_product_need_filter()
		),
		$link
	);
}

/**
 * Retourne l'URL de la page en cours de consultation.
 *
 * @return string
 */
function timacagro_current_url()
{
	global $wp;
	return home_url( add_query_arg( array(), $wp->request ) );
}

/**
 * Retourne l'URL fourni en paramètre avec le paramètre GET "context-page"
 *
 * @return string
 */
function timacagro_context_url( $link )
{
	return add_query_arg( 'context-page', timacagro_current_url(), $link );
}

/**
 * Notification de produits en attente.
 *
 * @return void
 */
function timacagro_notification_menu() {
	add_options_page(
		'Notifcations',
		'Notifcations',
		'manage_options',
		'timacagro-notifications-settings',
		'timacagro_notification_options'
	);
	add_action( 'admin_init', 'timacagro_register_notifications_settings' );
}
if ( is_admin() ){
	add_action( 'admin_menu', 'timacagro_notification_menu' );
}

/**
 * Nouveau settings.
 *
 * @return void
 */
function timacagro_register_notifications_settings() {
	//register our settings
	register_setting( 'timacagro_notification-group', 'timacagro_notification_admin_email' );
}


function timacagro_notification_options() {
?>
	<div class="wrap">
	<h2>Notifcations</h2>
	<p>Qui doit recevoir les emails de notifications de produits en attente de relecture ?</p>
	<form method="post" action="options.php">
		<?php settings_fields( 'timacagro_notification-group' ); ?>
		<?php do_settings_sections( 'timacagro_notification-group' ); ?>
		<table class="form-table">
			<tr valign="top">
        	<th scope="row">Adresse email:</th>
        	<td><input type="text" name="timacagro_notification_admin_email" class="regular-text" value="<?php echo get_option('timacagro_notification_admin_email'); ?>" /></td>
        	</tr>
		</table>
		<?php submit_button(); ?>
	</form>
	</div>
<?php
}

add_action('transition_post_status','timacagro_pending_submission_send_email', 10, 3 );
function timacagro_pending_submission_send_email( $new_status, $old_status, $post ) {

	// Notifiy Admin that Contributor has writen a post
	if ($new_status == 'pending' && user_can($post->post_author, 'edit_products') && !user_can($post->post_author, 'publish_products')) {
		$pending_submission_email = get_option('timacagro_notification_admin_email');
		$admins = (empty($pending_submission_email)) ? get_option('admin_email') : $pending_submission_email;
		$url = get_permalink($post->ID);
		$edit_link = get_edit_post_link($post->ID, '');
		$preview_link = get_permalink($post->ID) . '&preview=true';
		$username = get_userdata($post->post_author);
		$subject = 'Nouveau produit en attente de relecture : "' . $post->post_title . '"';
		$message = 'Un nouveau produit est en attente de relecture avant publication.';
		$message .= "\r\n\r\n";
		$message .= "Auteur : $username->user_login\r\n";
		$message .= "Titre : $post->post_title";
		$message .= "\r\n\r\n";
		$message .= "Modifier le produit : $edit_link\r\n";
		$message .= "Aperçu : $preview_link";
		$result = wp_mail($admins, $subject, $message);
	}
}
