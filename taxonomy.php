<?php get_header() ?>

	<!-- Taxonomie product range -->

	<div class="page taxonomy">

		<?php get_template_part( 'template-parts/header', 'taxonomy' ) ?>

		<?php get_template_part( 'template-parts/content', 'taxonomy' ) ?>

		<?php if ( have_posts() ) : ?>

			<div class="l-section l-section--stack">

				<div class="l-container">

					<div class="cards">

						<?php while ( have_posts() ) : the_post() ?>

							<?php get_template_part( 'template-parts/loop' ) ?>

						<?php endwhile ?>

					</div>

				</div>

			</div>

			<?php the_posts_pagination() ?>

		<?php endif  ?>

	</div>

<?php get_footer() ?>