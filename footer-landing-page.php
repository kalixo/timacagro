<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 */
?>

	</div><!-- .site__content -->

	<footer class="site__footer footer footer--landing-page" role="contentinfo">
		<div class="l-container">
			<div class="footer__prefooter">
				<?php if ( get_field( 'lp-footer' ) ) : ?>
					<div class="footer__content">
						<?php the_field( 'lp-footer' ) ?>
					</div>
				<?php endif ?>

				<?php if ( have_rows( 'footer-socials', 'options' ) ) : ?>
					<ul class="footer__social social">
						<?php while ( have_rows( 'footer-socials', 'options' ) ) : the_row() ?>
							<li class="social__item">
								<a href="<?php the_sub_field( 'link' ) ?>" title="<?php the_sub_field( 'name' ) ?>" target="_blank"
									onclick="ga('send','event','lien-footer-<?php echo strtolower( get_sub_field( 'name' ) ) ?>','clic','clic-sortant',1);">
									<span class="icon icon--<?php the_sub_field( 'icon' ) ?>"></span>
								</a>
							</li>
						<?php endwhile ?>
					</ul>
				<?php endif ?>
			</div>
		</div>
		<div class="footer__copyright">
			<?php the_field( 'footer-copyright', 'options' ) ?>
		</div>
	</footer><!-- .site__footer -->
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
