<?php
/**
 * Template Name: Landing Page
 */

get_header( 'landing-page' ) ?>

	<div class="page">

		<?php while ( have_posts() ) : the_post() ?>

			<?php get_template_part( 'template-parts/content', 'landing-page' ) ?>

		<?php endwhile ?>

	</div>

<?php get_footer( 'landing-page' );